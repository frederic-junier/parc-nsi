% Présentation de la spécialité NSI
% [![](images/logo-parc.png "logo")](https://lyceeduparc.fr/ldp/)
% Novembre 2021

# NSI ?
 
## Qu'est-ce que c'est ?

NSI désigne l'enseignement  de la spécialité  Numérique et Sciences Informatiques qui permet :

::: incremental
* d'aborder les bases de l'informatique
* de former les élèves à la pratique d'une démarche scientifique.
:::

## Les thèmes du programme

!["7 thèmes, source : Lycée Laennec - Pont L'Abbé"](images/7themes2.png "autre")

## Les compétences transversales 

La spécialité NSI permet de développer des compétences transversales : 

::: incremental
* faire preuve d'autonomie, d'initiative et de créativité
* présenter un problème ou sa solution
* coopérer au sein d'une équipe
* rechercher une information et partager des ressources
* faire un usage responsable et critique de l'information
:::

# Pourquoi choisir NSI ?

## On peut choisir NSI



:::incremental

*  comme complément à toute autre spécialité car les compétences informatiques sont un atout dans tous les domaines, des sciences exactes aux sciences sociales. 
*   comme préparation à des études d’informatique. Dans ce cas,  il est  conseillé d’associer  NSI avec la spécialité Mathématiques en première et la spécialité Mathématiques ou  l’option Mathématiques complémentaires en terminale.
*  de nombreux débouchés (infographie _Delphine Chavanon_)
:::


## 

!["Les débouchés, source : Delphine Chavanon"](images/orientation_post_nsi_reduce.png "les débouchés")


## Débouchés professionnels

Une formation en informatique sera un atout pour trouver un emploi dans un monde professionnel en pleine révolution digitale.

## Informatique => emploi

!["source : world economic forum"](images/jobsr.png "Informatique => emploi")


# Évaluation

## Abandon de la spécialité en fin de première

Si l’élève abandonne la spécialité NSI en fin de première, sa moyenne annuelle est conservée pour le bac avec un coefficient 8, voir [Eduscol](https://www.education.gouv.fr/ajustement-pour-le-baccalaureat-general-et-technologique-compter-de-la-session-2022-324134)

## Épreuves terminales 


Si l’élève a conservé la spécialité NSI en terminale, (voir [eduscol](https://eduscol.education.fr/cid144156/nsi-bac-2021.html)) il passe deux épreuves terminales : une épreuve écrite de trois heures trente minutes et une épreuve sur machine d’une heure.

Le coefficient est de 16.


# Compléments 

## Projet niveau première : 

![[Programmation du jeu Bulls and Cows]([Bulls and Cows](https://en.wikipedia.org/wiki/Bulls_and_Cows))](images/bulls_cows.png "autre")


## Projet niveau terminale : 

<video controls loop autoplay>
 <source src="images/exploration-labyrinthe.mp4" type="video/mp4">
</video>

<p><em> Manipulation de graphes, exploration d'un labyrinthe (source : Laurent Abbal)</em> </p>


## Projet niveau terminale : 



<img src="images/rotation-dragon-3.gif" alt="dragons">

<p><em> Rotation d'une image : récursivité / diviser pour régner.</em> </p>

## [quandjepasselebac.education.fr/](http://quandjepasselebac.education.fr/la-specialite-numerique-et-sciences-informatiques-au-bac/)


<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.ac-lyon.fr/videos/embed/d13e73ea-69d0-47ad-b488-4ede0b430c9a" frameborder="0" allowfullscreen></iframe>

## Contacts 

* Professeurs référents : M.Duclosson et M.Junier
* [Mail](mailto:frederic.junier@ac-lyon.fr)
* Site web : [https://frederic-junier.org/NSI/premiere/](https://frederic-junier.org/NSI/premiere/)
