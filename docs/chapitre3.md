---
title:  Chapitre 3  fonctions, spécification et mise au point, portée d'une variable
---



Cours de Frédéric Junier.

## Cours

* [Pdf du cours sur  fonctions, spécification et mise au point, portée d'une variable](chapitre5/Chapitre5-Fonctions-PorteeVariable-2020.pdf)

* Exemples du cours :
    * [Activité Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/23a7-84924)
    * [version pdf](chapitre5/corrige-cours/1NSI_Fonctions_Specification_Tests.pdf)
    * [version html](chapitre5/corrige-cours/1NSI_Fonctions_Specification_Tests.html)
    * [version Jupyter Notebook](chapitre5/corrige-cours/1NSI_Fonctions_Specification_Tests.ipynb)


* TD sur Rurple :
    * [énoncé](chapitre5/TD-Rurple/PremiereNSI-TD-Rurple-2021.pdf)
    * [archive avec le matériel](chapitre5/TD-Rurple/RurpleEleve.zip)
    * [corrigé](chapitre5/TD-Rurple/ressources/corrige-Rurple-TD-Fonctions.py)

  
* Règle __LEGB__ de portée d'une variable sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/96ec6Bc4iXA?list=PL2CXLryTKuwwhivE1UO4Jg5DhU-ALAoXc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* Différence entre `import math` et `from math import pi` sur la chaîne YouTube du [Cours Python 3 d'Arnaud Legout](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA):


??? video

    <iframe width="1239" height="697" src="https://www.youtube.com/embed/qeFLl4fKuf8?list=PL2CXLryTKuwzvTABc_Ha48Xf3pj12P4ij" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
