---
title:  Chapitre 8  recherches séquentielle et dichotomique
---



Cours de Pierre Duclosson et Frédéric Junier.


## Cours 

* [Cours version pdf](chapitre10/cours/NSI-Recherches-2021.pdf)
* [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/8c8a-224776)
* [Exercices du cours, carnet Notebook Capytale, correction](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/7c74-224825)
* [Exercices du cours, correction version `.py`](chapitre10/cours/ressources/Cours_Recherche_Premiere_NSI_2021_Correction.py)



## Exercices supplémentaires

* [Exercice Tas de Graine Castor 2017](https://concours.castor-informatique.fr/index.php?team=castor2017)
* [Énoncé des exercices](chapitre10/cours/exercice.md)
* [Corrigés des exercices](https://mybinder.org/v2/gh/parc-nsi/premiere-nsi/master?filepath=chapitre10/Corrige_Exercices_10022020.ipynb)

## Tutoriels vidéo 

??? video "Jeu de la devinette"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="devinette_dichotomie" src="https://tube.ac-lyon.fr/videos/embed/f8029a1a-a682-4371-bf3a-941b06a25cbe" frameborder="0" allowfullscreen></iframe>
