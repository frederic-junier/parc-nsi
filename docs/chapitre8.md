---
title:  Chapitre 6  représentation des entiers
layout: parc
---





Cours de Pierre Duclosson et Frédéric Junier.



## Cours / TP

* [Cours / TP en version pdf](chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
* [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c/2076-172112)
* [Corrigé du cours / TP en version pdf](chapitre8/Cours_Representation_Entiers_Correction.pdf)
* [Corrigé des exercices de calcul sans python](chapitre8/Chapitre6-ReprésentationEntiers-2021V1-corrige.pdf)
* [Corrigé du cours en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a963-171873)


## Tutoriel vidéo 

??? video
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="entiers_signes_complements2" src="https://tube.ac-lyon.fr/videos/embed/e7951678-75af-4101-9e24-9574d18ce294" frameborder="0" allowfullscreen></iframe>
