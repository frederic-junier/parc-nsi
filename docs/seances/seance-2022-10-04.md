---
title: Séance du 04/10/2022
---

# Séance du 04/10/2022


## Présentations orales du mini-projet 1

![alt](./ressources/Assurancetourix.webp)

## Projet _Lapins crétins_

![alt](./ressources/lapin.png)

* On poursuit le projet : [matériel](../Projets/LapinsCretins/materiel.zip) :
  * Exercice 1 : liste des chiffres d'un entier en base dix, spécification d'une fonction, tests
  * Exercice 2 : changement de base `decimal_vers_ternaire`


## Chapitre 3 : fonctions, spécification et tests

* [Cours](../chapitre3.md)
* [Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer)

## Appel à candidature

Souhaitez-vous participer à la [nuit du code](https://www.nuitducode.net/) ?

![alt](./ressources/nuitducode-scratch-python.svg)

## A faire pour jeudi 6/10 :


!!! abstract "France IOI"

    Dans le chapitre 7 "Conditions avancées, opérateurs booléens"  chercher l'exercice suivant :

    * [L'espion est démasqué](http://www.france-ioi.org/algo/task.php?idChapter=648&idTask=1982)
