---
title: Séance du 06/04/2023
---

# Séance du 06/04/2023


## Automatismes 

!!! question

    Convertir au format IEE 754 le décimal $-12.4609375$.

    [Correction de cet exemple](../chapitre16/ressources/corrige-conversion-flottant-2023-04-06.pdf)

    Lien vers  un  [exemple](https://frederic-junier.gitlab.io/parc-nsi/chapitre16/ressources/conversion-decimal-formatiee754.pdf)


## Chapitre Correction

* [Cours version pdf](../chapitre20/1NSI-Cours-Correction-2023.pdf)
* [Correction du cours sur la correction (version 2021-2022)](../chapitre20/corrige/1NSI-Cours-Correction-2023-correction.pdf)


## Présentation de la spécialité NSI en terminale par deux élèbes de terminale

## Présentation du module pyxel et du projet de fin d'année

* [Tutoriel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)
* [Inscription pour le projet final](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
* [Fiche de suivi (pdf modifiable)](../Projets/Projets2023/ProjetFinal/Cadrage/Fiche_Suivi_Projet_Final_2023.pdf) à remplir individuellement et déposer au plus tard le mardi 25 avril dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1841>
* Projet (code + capsule audio de présentation individuelle de 5 minutes) à déposer au plus tard le dimanche 21 mai  dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843>



## Travail à faire

!!! task

    * Pour le mardi  de la rentrée, déposer sa fiche de suivi  (pdf modifiable) remplie dans dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1841>
