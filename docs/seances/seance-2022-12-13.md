---
title: Séance du 13/12/2022
---

# Séance du 13/12/2022


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## DS n°5 sur les tableaux

## Automatismes

*  [Automatismes 12 à 16 sur la ligne de commande](../automatismes/automatismes-2021-2022.md)
*  Memento shell
  * [Memento shell version pdf](../chapitre9/memento-shell/memento-shell-.pdf)
  * [Memento shell version markdown](../chapitre9/memento-shell/memento-shell-git.md)
  * [Matériel  pour les exemples du memento](../chapitre9/memento-shell/sandbox.zip)

## Système d'exploitation

* Synthèse du  cours sur les systèmes d'exploitation et correction des exercices qu'il contient.
    * [Cours version pdf](../chapitre9/cours-systeme/systeme-cours-.pdf)
    * [Cours version markdown](../chapitre9/cours-systeme/systeme-cours-git.md)

## Travail à faire

* Pour jeudi 05/01 : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976) à compléter sur Capytale
