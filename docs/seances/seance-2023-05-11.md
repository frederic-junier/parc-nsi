---
title: Séance du 11/05/2023
---

# Séance du 11/05/2023

## Automatisme 

* Rendu de monnaie glouton : <https://capytale2.ac-paris.fr/web/c/bd4d-544334>
## Chapitre Algorithmes gloutons


* [Énoncé pdf](../chapitre24/TP-Gloutons-2022.pdf)
* [Matériel : squelette de code, fichiers csv, corrigé](../chapitre24/materiel.zip)
* Aujourd'hui on traite les problèmes de _planification de séance_ et du _sac à dos_.

  
## Travail sur le projet de fin d'année

* [Tutoriel pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)
* [Inscription pour le projet final](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
* [Fiche de suivi (pdf modifiable)](../Projets/Projets2023/ProjetFinal/Cadrage/Fiche_Suivi_Projet_Final_2023.pdf) à remplir individuellement et déposer au plus tard le mardi 25 avril dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1841>
* Projet (code + capsule audio de présentation individuelle de 5 minutes) à déposer au plus tard le dimanche 21 mai  dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843>



## Travail à faire

!!! task

    * Projet Final : Déposez au plus tard le dimanche 21 mai dans <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843> :
        * une archive au format zip avec votre code et toutes les ressources nécessaires
        * une capsule audio ou video individuelle de 5 minutes présentant le projet avec un focus sur une de vos contributions au projet : plan structuré avec introduction et conclusion
