---
title: Séance du 06/10/2022
---

# Séance du 06/10/2022


## Automatismes de programmation

!!! abstract "France IOI"

    Dans le chapitre 7 "Conditions avancées, opérateurs booléens"  correction de l'exercice suivant :

    * [L'espion est démasqué](http://www.france-ioi.org/algo/task.php?idChapter=648&idTask=1982)

## Projet _Lapins crétins_

![alt](./ressources/lapin.png)

* On poursuit le projet : [matériel](../Projets/LapinsCretins/materiel.zip) :
  * Exercice 2 : changement de base `decimal_vers_ternaire`
  * Exercice 4 : générer des symboles

## Chapitre 3 : fonctions, spécification et tests

* [Cours](../chapitre3.md)
* [Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer) : exercices 1, 2, 3, 4 et 5




## Pour mardi  11/10 :


!!! abstract "Entraînement à la programmation"

    * Chercher l'exercice [Département de chimie : mélange explosif](http://www.france-ioi.org/algo/task.php?idChapter=649&idTask=2058) du chapitre 7 "Conditions avancées, opérateurs booléens"
    * [Activité Capytale ➡️ cours fonctions](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer) : exercices 1, 2, 3, 4 et 5


## Pour jeudi 13/10 :


!!! abstract "Révisions pour le DS 2"

    * _Chapitre 1 : bases de programmation en Python_ : variables, calculs, structures conditionnelles; boucles `for` et `while`, fonctions 
    * _Chapitre 2 : HTML/CSS_, seule la connaissance de la synthèse de cours est exigible ➡️  QCM
    * _Chapitre 3 : fonctions, spécification et tests_ : revoir les exercices du cours faits dans le carnet [Capytale](https://capytale2.ac-paris.fr/web/c/23a7-84924/mcer)
    