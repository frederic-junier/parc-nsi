---
title: Séance du 28/02/2023
---

# Séance du 28/02/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## Automatismes 

Des corrigés de ces exercices sont disponibles [ici](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&sectionid=2#section-5)

* 👌  [Automate en dictionnaire](https://e-nsi.gitlab.io/pratique/N0/320-automate_0/sujet/)
*  ✍️  Tri par insertion : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=456>
*  ✍️ [Substitue](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1031)


## Chapitre Dictionnaires

* [Cours](../chapitre18/Cours/dictionnaires-cours-git.md)
* [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/8394-398180) avec exercices 2, 3 et 4 du cours + exercices sur le site  <https://e-nsi.gitlab.io/nsi-pratique/N1/antecedents/sujet/> + 2 exercices du site [Prologin](https://prologin.org/train/2010)

  
## Travail à faire

* Pour le jeudi 03/03 : DS sur les chapitres Fonctions booléennes et circuits logiques (QCM : table de vérité d'une porte logique AND, NOR, NAND, OR ou XOR, revoir [automatismes 11, 12 et 13](https://frederic-junier.gitlab.io/parc-nsi/automatismes/automatismes-2021-2022/), Tuples  et Tris (Algorithmique : tri par sélection et par insertion).