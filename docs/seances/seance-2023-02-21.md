---
title: Séance du 21/02/2023
---

# Séance du 21/02/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## Automatismes 

Des corrigés de ces exercices sont disponibles [ici](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&sectionid=2#section-5)

*  ✍️  Tri par sélection  :   <https://e-nsi.gitlab.io/pratique/N1/500-tri_selection/sujet/> 
*  ✍️   Insertion dans une liste triée : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=455>
*  ✍️  Tri par insertion : <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=456>

## Chapitre architecture de Von Neumann 

* [Lien vers le chapitre 17](../chapitre17.md)
* [Cours version pdf](../chapitre17/NSI-ArchitectureVonNeumann-Cours2022.pdf)
* __Premier temps :__ histoire de l'informatique et visionnage de quelques séquences de la video <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs> avec application aux exercices 1 et 2
* __Second temps (cours) :__ présentation de l'architecture de Von Neummann, de la hiérarchie des mémoires, des différents niveaux de langage entre l'homme et la machine :  faire l'exercice 3 mais pas les 4 et 5. 
* __Troisième temps (pratique) :__  Manipulation de l'[Emulateur Aqua](http://www.peterhigginson.co.uk/AQA/)
    * Ressources :
      * [Correction video de l'exercice 7](https://nuage03.apps.education.fr/index.php/s/PZmdbwns2wW2AJ8)
      * [Correction video de l'exercice 10](https://nuage03.apps.education.fr/index.php/s/zwPMMFXXm6gzbCt)


  
## Travail à faire

* Pour le jeudi 03/03 : DS sur les chapitres Fonctions booléennes et circuits logiques (QCM : table de vérité d'une porte logique AND, NOR, NAND, OR ou XOR), Tuples et  Chaînes de caractères (Programmation) et Tris (Algorithmique : tri par sélection et par insertion).