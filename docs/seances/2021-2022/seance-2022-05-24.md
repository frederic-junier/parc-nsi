---
title: Séance du 24/05/2022
---
# Interactions homme / machine dans un navigateur Web avec Javascript

* Le [cours](../chapitre23/javascript-git2.md) :
    * Présentation du langage [Javascript][Javascript] et de la programmation événementielle côté client : points de cours 1 et 2 puis exercices 1 et 2.
    * Approfondissement et rédaction de scripts et gestionnaires d'événements : exemples 1 et 2 puis point de cours 3.
    * Travaux pratiques [Javascript][Javascript] : exercices 3 et 4.
    * QCM de synthèse (exercice 5)


