---
title: Séance du 06/04/2022 
---

# Séance du 06/04/2022



## Chapitre traitement de données en tables


###  Cours 


* [Cours version pdf](../chapitre19/Cours/cours-tables-indexation-.pdf)
* [Cours version markdown](../chapitre19/Cours/cours-tables-indexation-git.md)

??? video "Correction de l'exercice 2"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="tables-donnees-cours" src="https://tube.ac-lyon.fr/videos/embed/181df983-1010-4442-8a72-a3a4cd14c6c6" frameborder="0" allowfullscreen></iframe>


###  TP  : Recherche et tri dans un dictionnaire

* [Énoncé du TP en Markdown](../chapitre19/TP-Recherche-Tri/tp-recherche-tri-git.md)
* [Énoncé du TP en pdf](../chapitre19/TP-Recherche-Tri/tp-recherche-tri-.pdf)
* [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/0bb5-484953)

## Présentation du projet de fin d'année, découverte du module pyxel


* [Tutoriel sur le module pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document de cadrage du projet final](../Projets/2021-2022/ProjetFinal/Cadrage/NSI_Presentation_Projet2022.pdf)
* [Inscription avec le mot de passe donné par l'enseignant](https://nuage-lyon.beta.education.fr/s/Fyf7EQgrpHSfCFP)



## Devoir commun

* [Entraînement sur les exercices avec juge en ligne sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=2&sectionid=3)
* Le mercredi 13 avril 2022.
* DS et corrigés donnés cette année : <https://nuage-lyon.beta.education.fr/s/sGHxPqMizZj9Txd>
