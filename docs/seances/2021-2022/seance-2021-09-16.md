---
title: Séance du 16/09/2021
---

# Séance du 16/09/2021

## Consignes et méthode de travail :

* Matériel :
    * Un classeur avec des feuilles pour écrire et des pochettes transparentes pour ranger les cours
    * Mettre le site <https://parc-nsi.github.io/premiere/> dans les favoris de son navigateur, navigation responsive adaptée aux smartphone : toutes les ressources (cours, corrigés) sont publiés et disponibles sur ce site. Trois rubriques à retenir :
        * La [progression](https://parc-nsi.github.io/premiere/) avec les liens vers les chapitres
        * La liste des [séances](https://parc-nsi.github.io/premiere/seances/) détaillées qui sont ensuite copiées dans Pronote.
        * La rubrique [Automatismes](https://parc-nsi.github.io/premiere/automatismes/) avec des liens vers des QCM externes et des exercices pour travailler les automatismes.
    * Une clef USB de 8 Go minimum, cet [article](https://www.boulanger.com/ref/872118) n'est pas cher.
    * Le manuel Hachette NSI version papier fourni par la région de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

* Méthode de travail :
    * D'une séance à l'autre : relire le cours, faire les exercices
    * Pendant la séance : alternance de temps d'activités et de synthèse, travail sur des projets    à rendre
    * Évaluations :
        * Rendu de mini-projet ou de projet plus conséquent (pendant les vacances) : travail en classe et à la maison en binôme, évaluations écrites ou orales
        * Formatives sous forme d'interrogations courtes (format QCM) ou d'exposés oral (histoire de l'informatique, synthèse de cours)
        * Sommatives sous forme de devoir d'une heure ou de TP noté


## Chapitre 1: constructions de bases en langage Python

!!! tip "France IOI Niveau 1 Chapitre 2  Répétitions d'instrutions"

    Retour sur quelques exercices du chapitre 2 _Répétitions d'instructions_ de [France IOI](http://www.france-ioi.org) :

    * [Mont Kailash](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1883), une [solution](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/france-ioi/niveau1/chapitre2/mont_kailash.py)
    * [Vendanges](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1884), une [solution](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/france-ioi/niveau1/chapitre2/vendanges.py)


!!! tip "TP2"

    On a fait le [TP2](../chapitre1/TP2/TP2.pdf) jusqu'à l'exercice 3. 
    Liens vers les corrigés :    
    * [corrigé pdf](../chapitre1/TP2/correction/Correction_TP2.pdf)
    * [corrigé .ipynb](../chapitre1/TP2/correction/Correction_TP2.ipynb)
    * [corrigé .py](https://gitlab.com/frederic-junier/parc-nsi/-/raw/master/docs/chapitre1/TP2/correction/Correction_TP2.py)


!!! question "Mini projet"

     Recherche en classe par binômes sur les  [sujets](../Projets/MiniProjets2021/Mini-Projet1/sujets/NSI-MiniProjets1-Sujets.pdf.

     Un exemple de fonction renvoyant un booléen :
 
    ~~~python
     def positive(temperature):
        """
        Teste si temperature >= -273

        Parameters
        ----------
        temperature : float

        Returns
        -------
        boolean

        """
        #précondition
        assert temperature >= -273, "valeur invalide"
        # if temperature >= 0:
        #     return True
        # else:
        #     return False
        return temperature >= 0 #plutôt
    ~~~

   





## A faire pour la semaine prochaine :

1. Sur [France ioi](http://www.france-ioi.org/) finir le chapitre 3 du niveau 1.
2. Travail sur le [mini projet 1](../Projets/MiniProjets2021/Mini-Projet1/sujets/NSI-MiniProjets1-Sujets.pdf) à rendre le jeudi 23/09 (présentation orale le mercredi 29/09).




