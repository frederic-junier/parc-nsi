---
title: Séance du 13/01/2022
---

# Séance du 13/01/2022



## Retour de l'évaluation sommative sur les chapitres ligne de commande et recherches séquentielle/dichotomique



## Chapitre 9 : algorithme de tri

Aujourd'hui on s'intéresse à la complexité du tri par insertion qui est un peu plus complexe que celle du tri par sélection !

On corrige aussi quelques exercices du QCM 1 à 8 page 113 du [manuel](https://mesmanuels.fr/acces-libre/3813624).

* [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
* [Matériel élève (squelette Python)](../chapitre11/materiel.zip)
* [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/967c-255137)
* [Cours/TP correction sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/29c9-255106)
* [Cours/TP correction version pdf](../chapitre11/ressources/Cours_Tris_Premiere_NSI_2021_Correction.pdf)


## Chapitre 10 : codage des caractères

Aujourd'hui on commence à manipuler des chaînes de caractères (inversion et test de palindrome, codage rot13) !

* [Cours version pdf](chapitre12/cours/NSI-CodageCaracteres-2020V1.pdf)
* [Exercices  du cours avec corrections sur Capytale ](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/9192-288733)


![Emojis, caractères de points de codes entre U+1F600 et U+1F64F](../chapitre12/cours/images/emojis.png)


## Travail sur le projet brainfuck

Aujourd'hui on prend le temps de travailler sur le projet.

* Pour le samedi 29 Janvier 2022, [projet brainfuck](../projets.md)