---
title: Séance du 05/01/2022
---

# Séance du 05/01/2022

## Automatismes

* Travail sur la [fiche de révisions](ressources/NSI-DS5-2022-Revisions.pdf)  et son [corrigé](ressources/NSI-DS5-Revisions-Corrige-2022V1.pdf) pour le DS du jeudi 06/01 ➡️  exercice 1
* [Automatismes 27 et 28 (recherche dichotomique)](../automatismes/automatismes-2021-2022.md)

## Chapitre 9 : algorithme de tri

* [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
* [Matériel élève (squelette Python)](../chapitre11/materiel.zip)
* [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/967c-255137)
* [Cours/TP correction sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/29c9-255106)
* [Cours/TP correction version pdf](../chapitre11/ressources/Cours_Tris_Premiere_NSI_2021_Correction.pdf)

## Évaluation sommative sur les chapitres ligne de commande et 

## Travail pour jeudi 06/01/2022

* Évaluation sommative (45 minutes) sur les recherches séquentielle / dichotomique  et la ligne de commandes : le jeudi 6/01/2022

## Travail sur le projet brainfuck


* Pour le samedi 22 Janvier 2022, [projet brainfuck](../projets.md)