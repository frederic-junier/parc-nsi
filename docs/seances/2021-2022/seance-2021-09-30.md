---
title: Séance du 30/09/2021
---

# Séance du 30/09/2021


## Chapitre 2 : révisions HTML/CSS

!!! info "Le point sur les langages HTML et CSS"

    * [Synthèse de cours](../chapitre2/memo/MemoHTML-CSS-2020.pdf)
    * Activités proposées dans [le mini-site portable](https://filesender.renater.fr/?s=download&token=27f5ebec-3d2f-4649-82f1-5a1080b71d9f)  :
        * Découverte de HTML dans `site/premiere_page_html/html_balises.html` faire les exercices  3, 4 et 5 : lire les consignes sur le mini-site et traiter les exercices dans Capytale :
            * [exercice 3](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/0f0c-61116) et sa [correction](../chapitre2/corrige-html/os.html)
            * [exercice 4](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/6caa-61110) et sa [correction](../chapitre2/corrige-html/os_versions.html)
            * [exercice 5](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/6226-61112) et sa [correction](../chapitre2/corrige-html/von_neumann.html)
            * [exercice 6](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/29bf-61114) et sa [correction](../chapitre2/corrige-html/composants.html)
        * Découverte de CSS : 
            * [activité dans Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/144e-76789)  et sa [correction](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/c60f-76867)


## Évaluation formative n°1 sur les constructions de base en Python 

Affectations, conditionnelles, boucles, fonctions.

## A faire pour la semaine prochaine :

1. Sur [France ioi](http://www.france-ioi.org/) finir le chapitre 4 et avancer le chapitre 5 jusqu'à la moitié.
2. S'inscrire sur la plateforme [Upylab](https://upylab2.ulb.ac.be) avec le lien fourni et si besoin s'entraîner sur les deux premiers exercices des chapitres 1 (Variables), 2 (Conditionnelle), 3 (boucle for), 4 (boucle while) et 5 (fonctions)


## A faire pour le 21/10/2021 :

* Projet HTML/CSS de réalisation d'un mini-site sur un thème en relation avec l'histoire de l'informatique. Chosir un sujet par binôme.
* Le cahier des charges, les [sujets](../sujets-html-css.md) et des exemples de ressources ont été proposés aux élèves.
* [Modèle de site fourni](../Projets/Projets2020/HTML-CSS-Histoire/modele.zip)
