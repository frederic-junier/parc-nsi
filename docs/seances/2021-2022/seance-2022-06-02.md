---
title: Séance du 02/06/2022
---

# Chapitre 25 : internet et réseau TCP/IP

* [cours](../chapitre25/reseau-cours-git.md) et activités avec le logiciel de simulation [Filius](https://www.lernsoftware-filius.de/downloads/Setup/filius_1.14.1_all.deb) :
    -   [Réseau informatique](../chapitre25/reseau-cours-git/index.html#reseau-informatique)
        -   [Histoire]../chapitre25/reseau-cours-git/index.html#histoire)
        -   [Terminologie et classification des réseaux](../chapitre25/reseau-cours-git/index.html#terminologie-et-classification-des-reseaux)
    -   [Le modèle en couches](../chapitre25/reseau-cours-git/index.html#le-modele-en-couches)
        -   [Découpage des données en paquets](../chapitre25/reseau-cours-git/index.html#decoupage-des-donnees-en-paquets)
        -   [Modèle en couches et encapsulation des données](../chapitre25/reseau-cours-git/index.html#modele-en-couches-et-encapsulation-des-donnees)
        -   [La couche liaison du modèle  TCP/IP](../chapitre25/reseau-cours-git/index.html#la-couche-liaison-du-modele-tcpip)
        -   [La couche application et le service DNS](../chapitre25/reseau-cours-git/index.html#la-couche-liaison-du-modele-tcpip/#la-couche-application-du-modele-tcpip)
* On termine avec un [problème type bac](../chapitre25/reseau-cours-git/index.html#la-couche-liaison-du-modele-tcpip/#probleme-type-bac)