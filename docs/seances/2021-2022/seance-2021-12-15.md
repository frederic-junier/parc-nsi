---
title: Séance du 15/12/2021
---

# Séance du 15/12/2021



## Recherche dans un tableau : séquentielle ou dichotomique

* [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
* [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/8c8a-224776)
* [Exercices du cours, carnet Notebook Capytale, correction](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/7c74-224825)
* [Exercices du cours, correction version `.py`](../chapitre10/cours/ressources/Cours_Recherche_Premiere_NSI_2021_Correction.py)


??? video "Jeu de la devinette"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="devinette_dichotomie" src="https://tube.ac-lyon.fr/videos/embed/f8029a1a-a682-4371-bf3a-941b06a25cbe" frameborder="0" allowfullscreen></iframe>




## Travail sur le projet brainfuck


* Pour le jeudi 20 Janvier 2022, [projet brainfuck](../projets.md)


## Travail pour jeudi 20/12


* Simulation d'épreuve pratique sur machine, deux exercices :
    * un exercice d'application directe des connaissances de base : recherche dans tableau (d'élement, de maximum ou minimum), changement de base
    * un exercice avec prise d'initiative et mise en place d'une solution algorithmique à un problème
