---
title: Séance du 24/11/2021
---

# Séance du 24/11/2021

## Automatismes :

* [Automatisme 9](../automatismes/automatismes-2021-2022.md)



##  Chapitre 6 : représentation des entiers

* [Cours / TP en version pdf](../chapitre8/Chapitre6-ReprésentationEntiers-2021V1.pdf)
* [Cours / TP en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/2076-172112)
* [Matériel en archive zip (fichiers Python, Notebook)](../chapitre8/materiel.zip)
* [Corrigé du cours / TP en version pdf](../chapitre8/Cours_Representation_Entiers_Correction.pdf)
* [Corrigé du cours en notebook Python sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a963-171873)




## Travail pour jeudi 25/11

* QCM sur la représentations des entiers:
    * [Énoncé](https://genumsi.inria.fr/qcm.php?h=9ecc083484829c2ee620207e0d28e5d8)
    * [Corrigé](https://genumsi.inria.fr/qcm-corrige.php?cle=MTM7NzA7OTQ7MTg1OzE5Mjs0NjA7MTM1NDsxNDE0OzE2MzA7MTY1OQ==)
* [Automatisme 8](../automatismes/automatismes-2021-2022.md)


## Révisions pour mercredi 01/12

* Interrogation écrite (30 minutes) sur les tableaux à 1 ou 2 dimensions et la représentations des entiers (ce qui a été vu aujourd'hui uniquement)

