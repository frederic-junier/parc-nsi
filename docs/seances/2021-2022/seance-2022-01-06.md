---
title: Séance du 06/01/2022
---

# Séance du 06/01/2022

## Chapitre 9 : algorithme de tri

* [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
* [Matériel élève (squelette Python)](../chapitre11/materiel.zip)
* [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/967c-255137)
* [Cours/TP correction sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/29c9-255106)
* [Cours/TP correction version pdf](../chapitre11/ressources/Cours_Tris_Premiere_NSI_2021_Correction.pdf)


## Évaluation sommative sur les chapitres ligne de commande et recherches séquentielle/dichotomique

## Travail pour la semaine prochaine

* Pour mercredi 12/01, finir  les exos 9 et 10 du chapitre sur les tris (tri par insertion)
* Pas de cours jeudi 13/01 (sortie pédagogique)

## Travail sur le projet brainfuck


* Pour le samedi 22 Janvier 2022, [projet brainfuck](../projets.md)