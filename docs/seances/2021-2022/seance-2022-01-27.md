---
title: Séance du 27/01/2022
---

# Séance du 27/01/2022

## Automatismes

* [Automatisme 7](../automatismes/automatismes-2021-2022.md)

## Chapitre 11 Fonctions booléennes et circuits logiques


On présente les portes logiques et on formalise la notion de fonction booléenne déjà rencontré dans les structures conditionnelles en programmation.

Si le logiciel [logisim](http://www.cburch.com/logisim/download.html) n'est pas disponible on utilise le simulateur de porte logique en ligne <https://logic.ly/demo/>.

L'objectif du jour est de construire trois portes logiques : `XOR`, demi-additionneur binaire et additionneur binaire.

* [Cours version pdf](../chapitre13/cours-circuits-logiques-.pdf)
* [Cours version markdown](../chapitre13/cours-circuits-logiques-git2.md)  
* [Archive avec les circuits logisim](../chapitre13/circuits_logisim.zip)  
* [Corrigés des exercices 1 à 6](../chapitre13/corrige/corrige-circuits-logiques.pdf)


## Chapitre Complexité


* [Cours version pdf](../chapitre14/cours_complexite_2022.pdf)
* [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/38c6-329322)

## Evaluation sommative du jeudi 03/02

Réviser les chapitres recherche séquentielle et dichotomique, tris et chaînes de caractères.

## Travail sur le projet brainfuck


* Date de remise : pour le samedi 29 Janvier 2022, [projet brainfuck](../projets.md)


??? video  "Vidéo tutoriel 1"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="branfuck_partie1" src="https://tube.ac-lyon.fr/videos/embed/d97f5c67-f791-43d3-bb96-ddf6f4eb5be3" frameborder="0" allowfullscreen></iframe>



??? video  "Vidéo tutoriel 2"

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="brainfuck_partie2" src="https://tube.ac-lyon.fr/videos/embed/9e135a4e-3416-4d6d-b4b1-2525ba2f1923" frameborder="0" allowfullscreen></iframe>



