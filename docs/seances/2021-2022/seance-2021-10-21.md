---
title: Séance du 21/10/2021
---

# Séance du 21/10/2021

## Rattrapage du DS n°2

##  Chapitre 4 : tableaux à une dimension

* [Cours](../chapitre4.md) :
    * Aliasing
    * Passage de tableau en paramètre d'une fonction
    * Méthodes de tableaux dynamiques
    * Synthèse

* [Exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/68ac-101047)

* Sur  [https://upylab2.ulb.ac.be](https://upylab2.ulb.ac.be)  :  résoudre les exercices du chapitre tableaux :
    * Opérations élémentaires sur les tableaux épisode 1
    * Somme des éléments d'un tableau de nombres
 



## Pour mercredi 10/11

* Interrogation écrite courte (20 minutes) sur le [Chapitre 4 Tableaux](../chapitre4.md)
* Traiter le [chapitre Structures avancées du niveau 1 de France IOI](http://www.france-ioi.org/algo/chapter.php?idChapter=647)



## Projet jeu de Nim à rendre le samedi 20/11

* [Énoncé](../Projets/Nim/Projet1-Nim-2021.pdf)
* [Archive avec squelettes de codes](../Projets/Nim/squelette_eleve.zip)


