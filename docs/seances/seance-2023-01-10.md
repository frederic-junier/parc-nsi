---
title: Séance du 10/01/2023
---

# Séance du 09/01/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## Automatismes

* [Recherche séquentielle à coder sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=535)
* [Recherche dichotomique à coder sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=528)

## Algorithmes de Tri :

Aujourd'hui au programme :  _tri par sélection_  : exercice 4 à 8 : déouverte de l'algorithme, implémentation, terminaison, correction et complexité.

* [Cours/TP version pdf](../chapitre11/1NSI-Cours-Tris-2021V1.pdf)
* [Matériel élève (squelette Python)](../chapitre11/materiel.zip)
* [Cours/TP sur Capytale](https://capytale2.ac-paris.fr/web/c/967c-255137)
* [Cours/TP correction sur Capytale](https://capytale2.ac-paris.fr/web/c/29c9-255106)
* [Cours/TP correction version pdf](../chapitre11/ressources/Cours_Tris_Premiere_NSI_2021_Correction.pdf)


Pour visualiser le déroulement d'un tri par sélection : <http://fred.boissac.free.fr/AnimsJS/Dariush_Tris/index.html>


Pour s'entraîner :

* [Tri par sélection décroissant sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=460)
* [Tri par sélection sur Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=532)
* [Tri par sélection sur e-nsi/pratique](https://e-nsi.gitlab.io/pratique/N1/500-tri_selection/sujet/)
* Deux exercices (plus longue suiste croissante et médian d'un tableau) dans le fichier Capytale [Sélection d'exercices Prologin](https://capytale2.ac-paris.fr/web/c/154e-1189828)

## Travail à faire

* Pour jeudi 12/01 : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976) à compléter sur Capytale
* Pour jeudi 19/01 : DS 6 sur les chapitres [Ligne de commande](https://frederic-junier.gitlab.io/parc-nsi/chapitre9/memento-shell/memento-shell-git/), [Recherche séquentielle et Dichotomique](https://frederic-junier.gitlab.io/parc-nsi/chapitre10/) et [Algorithmes de Tris](https://frederic-junier.gitlab.io/parc-nsi/chapitre11/). Un DS de révision est disponible sur Moodle.
