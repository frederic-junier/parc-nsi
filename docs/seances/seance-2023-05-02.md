---
title: Séance du 02/05/2023
---

# Séance du  02/05/2023


## Chapitre traitement de données en tables


###  Cours 

Suivre le polycopié de cours et traiter les exemples depuis un IDE Python à partir de l'archive.

* [Cours version pdf](../chapitre19/Cours/cours-tables-indexation-.pdf)
* [Cours version markdown](../chapitre19/Cours/cours-tables-indexation-git.md)
* [Carnet  Capytale avec les exercices de code](https://capytale2.ac-paris.fr/web/c/ae9d-1603777)

###  TP  Capytale  Recherche et tri dans une table

Ouvrir le  carnet Capytale pour traiter le TP  `TP_Recherche_Tris_Eleve.py`, compléter les étapes, appeler le professeur pour aide / validation. Dans chaque étape un test unitaire est fourni pour vérifier son code en autonomie.

* [Énoncé du TP](../chapitre19/TP-Recherche-Tri/tp-recherche-tri-git.md)
* [Carnet  Capytale avec les exercices](https://capytale2.ac-paris.fr/web/c/0bb5-484953)
* [Ressources pour le TP](../chapitre19/TP-Recherche-Tri/Ressources.zip)
* [Lien vers le chapitre 19](../chapitre19.md)
* 
  
###  TP  Capytale  Fusion de tables

* [TP version pdf](../chapitre19/TP-Fusion/tp-fusion-.pdf)
* [TP version markdown](../chapitre19/TP-Fusion/tp-fusion-git.md)
* [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/50a5-11200)
* [Ressources pour le TP](../chapitre19/TP-Fusion/Ressources/materiel_tp_fusion.zip)

* 
  
## Travail sur le projet de fin d'année

* [Tutoriel pyxel](../Projets/Pyxel/decouverte_pyxel.md)
* [Document](https://frederic-junier.gitlab.io/parc-nsi/Projets/Projets2023/ProjetFinal/Cadrage/ressources.zip)
* [Inscription pour le projet final](https://nuage03.apps.education.fr/index.php/s/nk7wGPA8S78BsqP)
* [Fiche de suivi (pdf modifiable)](../Projets/Projets2023/ProjetFinal/Cadrage/Fiche_Suivi_Projet_Final_2023.pdf) à remplir individuellement et déposer au plus tard le mardi 25 avril dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1841>
* Projet (code + capsule audio de présentation individuelle de 5 minutes) à déposer au plus tard le dimanche 21 mai  dans l'espace de dépôt Moodle <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843>



## Travail à faire

!!! task

    * Pour mardi 09/05, interrogation écrite courte sur les  chapitres Tables (connaissance des dictionnaires nécessaire) et Correction (uniquement la partie terminaison et les variants de boucles).
    * Projet Final : Déposez au plus tard le dimanche 21 mai dans <https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1843> :
        * une archive au format zip avec votre code et toutes les ressources nécessaires
        * une capsule audio ou video individuelle de 5 minutes présentant le projet avec un focus sur une de vos contributions au projet : plan structuré avec introduction et conclusion
