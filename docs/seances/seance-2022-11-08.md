---
title: Séance du 08/11/2022
---

# Séance du 08/11/2022


## Brouillon séance du jour

[brouillon sur Capytale](https://capytale2.ac-paris.fr/web/c/b560-918945)

## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé

## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## Retour du DS n°3


##  Chapitre 4 : tableaux à une dimension

Les élèves à l'aise avancent en autonomie les autres sont guidés par l'enseignant.

* Ressources :
  * [Cours](../chapitre4.md) 
  * [Exemples du cours en version notebook (Capytale)](https://capytale2.ac-paris.fr/web/c/68ac-101047)
* Révisions de la notion 1 : Construction d'un tableau 
  *  Exercices avec juge en ligne :
     *  👌 [Tableau en compréhension épisode 1](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=14)
*  Notion 3 : parcours de tableaux par index ou valeur
  *  [Cours](../chapitre4.md)  : paragraphe 7 et exercice 5
  *  Exercices avec juge en ligne :
     * 👌 [Somme des éléments d'un tableau]([Opérations élémentaires sur les tableaux épisode 1](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1341))
     * 👌 [Moyenne d'un tableau](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=453)
     *  ✍️ [Recherche d'un élément dans un tableau](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=442)
     *  ✍️ [Index de dernière occurrence](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=526)
     * ✍️  [Codage par différence](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=449)
     * ✍️ [Liste des indices des occurrences](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=533)
* Notion 4 : Aliasing
  * Aliasing et copie de tableaux :
    * [Cours](../chapitre4.md)  : paragraphe 8 

## Pours les costauds 

* Niveau 💪, faire les exercices suivants :
    * _Tous différents_, recherche de doublons dans un tableau : <https://e-nsi.gitlab.io/pratique/N1/420-tous_differents/sujet/>
    * _Nombre d'inversions dans un tableau_, <https://e-nsi.gitlab.io/pratique/N1/450-inversions_1/sujet/>


## Pour jeudi :

* Niveau 👌 et ✍️ , faire les exercices suivants :
    *  👌 [Somme des éléments d'un tableau]([Opérations élémentaires sur les tableaux épisode 1](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1341))
    * 👌 [Moyenne d'un tableau](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=453)
    *  ✍️ [Recherche d'un élément dans un tableau](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=442)
    *  ✍️ [Index de dernière occurrence](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=526)

* Niveau 💪, faire les exercices suivants :
    * _Premier minimum local_ : <https://e-nsi.gitlab.io/pratique/N1/122-premier_minimum/sujet/>
    * _Occurrences du minimum_, <https://e-nsi.gitlab.io/pratique/N1/130-occurrences_du_mini/sujet/>
