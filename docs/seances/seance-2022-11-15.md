---
title: Séance du 15/11/2022
---

# Séance du 15/11/2022



## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé

## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.


##  Automatismes sur les  tableaux à une dimension


* Automatismes 9,10 et 11 sur la page [automatismes](https://frederic-junier.gitlab.io/parc-nsi/automatismes/automatismes-2021-2022/)
* Exercices avec juge en ligne :
  * 👌 [Somme des éléments d'un tableau](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=1341)
  * ✍️ [Liste des indices des occurrences](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/lti/view.php?id=533) 

## Projet _Lapins crétins_

![alt](./ressources/lapin.png)

* On poursuit le projet étape 2 : [matériel](../Projets/LapinsCretins/materiel.zip) :
  * Exercice 7 : non évalué corrigé en classe
  * Exercice 8 : évalué, recherche individuelle


##  Chapitre 5 : tableaux à deux dimensions / Traitement d'images

* [Cours / TP en version pdf](../chapitre7/NSI-Images-Tableaux2d--2021V2.pdf)
  
* [Cours / TP en version notebook sur Capytale (avec corrigés)](https://capytale2.ac-paris.fr/web/c/7ad3-163223)

* [Matériel élèves (pour travailler hors connexion)](../chapitre7/Images-Tableaux2d-materiel.zip)

## Travail à faire

* Pour jeudi  :
  * Interrogation de 45 minutes sur le chapitre [Tableaux à une dimension](../chapitre4.md)
  * [DS de l'an dernier pour s'entraîner](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/folder/view.php?id=1459)
* Pour le samedi 26/11 : rendu de l'étape 2 du projet lapins crétins, code à déposer sur [espace de dépôt Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/assign/view.php?id=1458&forceview=1)