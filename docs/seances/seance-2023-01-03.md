---
title: Séance du 03/01/2023
---

# Séance du 03/01/2023


## Codage des activités 

* 👌 : facile, maîtrise élémentaire du cours
* ✍️   : difficulté moyenne, bonne maîtrise  du cours
* 💪  :  difficile, niveau avancé



## AP NSI

Vérifiez sur Pronote si vous êtes inscrit dans le groupe d'AP qui se réunira de 13 à 14 en salle 715.

## Automatismes

*  [QCM Genumsi sur la ligne de commandes](https://genumsi.inria.fr/qcm.php?h=629a91f29647762dda098d895fcb2d33)

## Recherche dans un tableau : séquentielle ou dichotomique

* [Cours version pdf](../chapitre10/cours/NSI-Recherches-2021.pdf)
* [Exercices du cours, carnet Notebook Capytale](https://capytale2.ac-paris.fr/web/c/8c8a-224776)
* [Exercices du cours, correction version `.py`](../chapitre10/cours/ressources/Cours_Recherche_Premiere_NSI_2021_Correction.py)



## Travail à faire

* Pour jeudi 12/01 : [Projet Stéganographie](https://capytale2.ac-paris.fr/web/c/69bf-1019976) à compléter sur Capytale
