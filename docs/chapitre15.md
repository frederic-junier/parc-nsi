---
title:  Chapitre 15  p-uplets
layout: parc
---




Cours de Frédéric Junier.




## Cours 

* [Cours version pdf](chapitre15/Cours/puplets-cours-.pdf)
* [Cours version markdown](chapitre15/Cours/puplets-cours-git.md)
* [Carnet Capytale avec exercices et corrigés](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/5745-345853)



## TP

* [TP version pdf](chapitre15/TP/NSI-Puplets-TP-2020V1.pdf)
* [TP sur Capytale](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/505b-359568)
* [materiel.zip](chapitre15/TP/materiel.zip)
* [Corrigé du TP version python](chapitre15/TP/corrige/TP_puplets_Corrigé.py)
* [Corrigé du TP version notebook](https://mybinder.org/v2/gh/parc-nsi/premiere-nsi/master?filepath=chapitre15/TP/corrige/TP_puplets_Corrigé.ipynb)


_XKCD 353 Python_

[![XKCD 353 Python](https://imgs.xkcd.com/comics/python.png)](https://www.explainxkcd.com/wiki/index.php/353:_Python)