#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  1 20:52:42 2022

@author: fjunier
"""
import random


def generateur_triplets(path, nb_triplets):
    """
    Génère un fichier texte au format CSV où chaque ligne contient
    un tuple  a,b,c  constitué de trois entiers aléatoires entre 0 et 1000

    Parametres
    ----------
    path (str) : chemin vers un fichier texte de format csv
    nb_triplets (int) : nombre de triplets

    Retour
    -------
    None.

    """
    f = open(path, "w", encoding="utf-8")
    for _ in range(nb_triplets):
        triplet = tuple(str(random.randint(1, 100)) for _ in range(3))
        f.write(','.join(triplet) + '\n')
    f.close()


def compte_triangle(path):
    """
    Compte le nombre de triangles parmi les triplets
    a,b,c contenus dans le fichier de chemin path

    Parameters
    ----------
    path (str) : chemin vers un fichier texte au format CSV

    Returns
    -------
    nb_triangle (int)

    """
    f = open(path,  encoding="utf-8")
    nb_triangle = 0
    for enreg in f:
        a, b, c = tuple(int(champ) for champ in enreg.split(','))
        if (a <= b + c) and (b <= a + c) and (c <= a + b):
            nb_triangle = nb_triangle + 1
    f.close()
    return nb_triangle


def compte_triplets_pythagoriciens(path):
    """
    Compte le nombre de triplets pythagoriciens parmi les triplets
    a,b,c contenus dans le fichier de chemin path

    Parameters
    ----------
    path (str) : chemin vers un fichier texte au format CSV

    Returns
    -------
    nb_triangle (int)

    """
    f = open(path,  encoding="utf-8")
    nb_pythagore = 0
    for enregistrement in f:
        a, b, c = tuple(int(champ) for champ in enregistrement.split(','))
        if ((b ** 2 == a ** 2 + c ** 2) or (a ** 2 == b ** 2 + c ** 2)
                or (c ** 2 == a ** 2 + b ** 2)):
            nb_pythagore += 1
    f.close()
    return nb_pythagore

# generateur_triplets("triplets.csv", 10000)


def test_unitaire():
    assert compte_triangle("triplets.csv") == 5223
    assert compte_triplets_pythagoriciens("triplets.csv") == 5
