---
title:  Progression
layout: parc
---


> Bienvenue sur le site de ressources de la classe de première NSI du [lycée du Parc à Lyon](https://lyceeduparc.fr/ldp/), l'enseignement est assuré par deux professeurs : Pierre Duclosson et Frédéric Junier.

> Nous utilisons le manuel Hachette NSI version papier  de référence `978-2-01-786630-5`, accessible en ligne sur <https://mesmanuels.fr/acces-libre/3813624>

>L'image `gif` ci-dessous présente  différentes étapes du déroulement d'un algorithme de rotation d'images inspiré d'un travail présenté par Laurent Abbal du lycée français de Tokyo. Le  programme assez court peut être réalisé par un élève  de terminale (récursivité, approche _diviser pour régner_).

>L'image source représente l'oeuvre _Matsuri Yatai Dragon_ du peintre japonais [Hokusai](https://en.wikipedia.org/wiki/en:Hokusai). Elle est dans le domaine public et disponible sur [https://commons.wikimedia.org](https://commons.wikimedia.org/wiki/File:Hokusai_Dragon.jpg).

[![Dragon](assets/rotation-dragon-2.gif "dragon-hokusai")](https://commons.wikimedia.org/wiki/File:Hokusai_Dragon.jpg)


# Présentation

* Voir notre [présentation de la spécialité NSI](presentation_parc_2021/presentation_spe_nsi.html).

# Programme 

* Le  [programme officiel](Programme/PPL18_Numerique-sciences-informatiques_SPE_1eGen_1025707.pdf)
* [Carte mentale du programme](Programme/PremièreNSI.jpg)



# Progression


* [Chapitre 1 : Constructions de base d'un langage de programmation](chapitre1.md)

* [Chapitre 2 : HTML/CSS](chapitre2.md)
* [Chapitre 3 : fonctions, spécification et mise au point, portée d'une variable](chapitre3.md)
* [Chapitre 4 : tableaux à une dimension](chapitre4.md)
* [Chapitre 5 : tableaux à deux dimensions / Images](chapitre5.md)
* [Chapitre 6 : représentation des entiers](chapitre8.md)
* [Chapitre 7 : système d'exploitation et ligne de commandes](chapitre9.md)
* [Chapitre 8 : recherche séquentielle ou dichotomique](chapitre10.md)
* [Chapitre 9 : algorithmes de tri](chapitre11.md)
* [Chapitre 10 : codage des caractères](chapitre12.md)
* [Chapitre 11 : circuits logiques et fonctions booléennes](chapitre13.md)
* [Chapitre 12 : complexité](chapitre14.md)
* [Chapitre 13 : p-uplets](chapitre15.md)
* [Chapitre 14 : architecture de Von Neumann](chapitre17.md)
* [Chapitre 15 : les dictionnaires](chapitre18.md)
* [Chapitre 16 : flottants](chapitre16.md)
* [Chapitre 17: correction d'algorithmes](chapitre17.md)
* [Chapitre 18 : traitement de données en table](chapitre19.md)
* [Chapitre 19 : Algorithmes gloutons](chapitre19.md) 
* [Chapitre 19 : Interaction Homme Machine sur le Web partie 1, protocole HTTP et formulaires](chapitre21.md)
* [Chapitre 21 : Interactions dans le navigateur, Javascript](chapitre23.md)  
* [Chapitre 22 : Réseau TCP/IP](chapitre25.md)  
* [Chapitre 23 : Algorithme de classification des K plus proches voisins](chapitre22.md) 
<!--  
-->