<!-- Définition des hyperliens  -->

{% include 'abbreviations.md' %}


# Crédits

*La version originale de cet énoncé de TP a été créée par Charles Poulmaire.*


# Contexte

!!! note "Quelques définitions"

    Le [système d'exploitation](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27exploitation) est l'ensemble des programmes qui permet aux autres programmes d'interagir avec les ressources matérielles (processeur, mémoire, périphériques d'entrée / sortie) sur un ordinateur. Il sert donc *d'intermédiaire* entre le *matériel* et le *logiciel* et assure la coordination, la sécurité et la stabilité d'un environnement  partagés par plusieurs programmes et plusieurs utilisateurs.

    Les systèmes d'exploitation les plus utilisés son [Windows][Windows], [MacOS][MacOS], [Linux][Linux] et [FreeBSD][FreeBSD], ces trois derniers étant dérivés du système [UNIX][UNIX].

    Un *interpréteur de commandes* ou *shell* est un programme qui sert d'intermédiaire entre l'utilisateur et le système d'exploitation : son *interface* d'entrée / sortie peut être graphique ou textuelle.

    Nous allons travailler sur  un *shell* avec interface textuelle nommé [BASH][BASH] qui est installé par défaut sur les systèmes [MacOS][MacOS] et [Linux][Linux].

    Pour approfondir le sujet, voir le [cours](../cours-systeme/systeme-cours-git.md)

!!! info "Support"

    Dans ce TP vous allez découvrir un certain nombres de commandes [UNIX][UNIX], utilisable dans le *shell*  à travers  un _escape game_ en ligne : [TERMINUS](http://luffah.xyz/bidules/Terminus). Vous trouverez ce jeu à l'url suivante : <http://luffah.xyz/bidules/Terminus/>


# Consignes


!!! tip "Consignes"

    Jouer à [TERMINUS](http://luffah.xyz/bidules/Terminus) et compléter au fil du jeu : 
    
    * un tableau  des commandes découvertes avec leur fonctionnalité et leur syntaxe
    * uen représentation du monde de [TERMINUS](http://luffah.xyz/bidules/Terminus) sous la forme d'une arborescence, comme celle d'un système de fichiers.

    On donne ci-dessous un tableau des commandes disponibles au début du jeu :

    |**Commande**|**Description**|**Syntaxe**|
    |:---:|:---:|:---:|
    |**cat**|Afficher dans la console|Saisir `cat Objet` ou `cat Personne`|
    |**ls**|Lister les éléments|Saisir `ls`|
    |**cd**|Changer de Destination|`cd ..` pour revenir à l'emplacement précédent|
    |**cd**|Changer de Destination|`cd  Salle` pour entrer dans Salle|
    |**cd**|Changer de Destination|`cd  ~` pour revenir au point de départ|

    Un exemple de plan de jeu :

    ![plan](ressources/plan-terminus.png){: .center}

    