annuaire = {}  #initialisation d'un dictionnaire vide
annuaire = { 'Victor' : '06 44 44 44 44'} #on peut aussi initialiser un dictionnaire par extension
print("Lecture du numéro de téléphone de Victor dans le dictionnaire annuaire ", annuaire['Victor'])
print("Modification du numéro de Victor dans le dictionnaire annuaire ")
annuaire['Victor'] = '06 33 33 33 33'
print("Ajout  du numéro de Valérie dans le dictionnaire annuaire ")
annuaire['Valérie'] = '06 22 22 22 22'
print("Affichage du dictionnaire annuaire")
print("Test d'appartenance de la clef 'Antoine' au dictionnaire annuaire", 'Antoine' in annuaire)
print("Test d'appartenance de la clef 'Valérie' au dictionnaire annuaire", 'Valérie' in annuaire)
print("Parcours du dictionnaire annuaire par clefs (ici les personnes) :")
for clef in annuaire:
    print("Clef : ", clef, " Valeur : ", annuaire[clef])
print("Parcours du dictionnaire annuaire par tuple (clef, valeur) (ici (personne, numéro)) :")
for clef, valeur in annuaire.items():
    print("Clef : ", clef, " Valeur : ", valeur)
