---
title:  TP scrabble
layout: parc
---


## Présentation de la structure de données _dictionnaires_

??? "video"

    [Présentation video par Pierre Marquestaut](https://peertube.lyceeconnecte.fr/videos/watch/86be0059-a3c1-41ec-952a-79dea6310c87)

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.lyceeconnecte.fr/videos/embed/86be0059-a3c1-41ec-952a-79dea6310c87" frameborder="0" allowfullscreen></iframe>
    
    
* En Python la structure de données __dictionnaire__ est implémentée par le type `dict`. Voici un  exemple de construction de dictionnaire en Python et de manipulation avec les opérations de base : définition, accès en lecture / écriture,test d'appartenance, parcours.


{{IDE("dictionnaire_intro")}}




## Une première utilisation des dictionnaires : résolution d'un problème sur CodinGame

1. Lire l'énoncé du problème sur la page <https://www.codingame.com/ide/puzzle/scrabble>.
2. Pour résoudre ce problème on propose de compléter le code ci-dessous :
    * Dans `spyder`, compléter chaque fonction   en respectant sa spécifications des fonctions et en vérifiant  les tests unitaires qui l'accompagnent. 
    * Compléter le code du programme principal.
    * Soumettre son code aux validateurs sur la page <https://www.codingame.com/ide/puzzle/scrabble>.


??? "Code"

    === "Squelette"

        Copier puis compléter le squelette ci-dessous dans `spyder`.

        ~~~python
        #dictionnaire des valeurs des lettres minuscules au scrabble
        valeur = {
            'a' : 1,
            'e' : 1,
            'i' : 1,
            'o' : 1,
            'n' : 1,
            'r' : 1,
            't' : 1,
            'l' : 1,
            's' : 1,
            'u' : 1,
            'd' : 2,
            'g' : 2,
            'b' : 3, 'c' : 3, 'm' : 3, 'p' : 3,
            'f' : 4, 'h' : 4, 'v' : 4, 'w' : 4, 'y' : 4,
            'k' : 5, 
            'j' : 8, 'x' : 8,
            'q' : 10, 'z' : 10}
        
        # %% Fonction signature

        def signature(mot):
            """Paramètre : mot de type str
            Valeur renvoyée : un dictionnaire représentant le nombre d'occurences
            de chaque lettre minuscule de l'alphabet dans mot""" 
            sig  = {}  #dictionnaire vide
            for c in mot:
                "à compléter"
            return sig

        #Tests unitaires de la fonction signature
        def test_signature():
            assert signature("") == {}
            assert signature("ananas") == {'a': 3, 'n': 2, 's': 1}
            assert signature("abcd") == {'a': 1, 'b': 1, 'c': 1, 'd': 1}

        # %% Fonction score_mot

        def score_mot(mot, valeur):
            """Paramètre : mot de type str, 
            Valeur renvoyée : score du mot au scrabble""" 
            s = 0
            "à compléter"
            return s

        #Tests unitaires de la fonction score_mot
        def test_score_mot():
            assert score_mot("") == 0
            assert score_mot("zazou") == 23
            assert score_mot("ananas") == 6


        # %% Fonction mot possible

        def mot_possible(mot, sig_lettres):
            """Paramètre : mot de type str
            sig_lettres de type dict représente la signature des lettres disponibles
            Valeur renvoyée : un booléen indiquant si sig_mot compatible avec  sig_lettres
            et donc si le mot peut être composé avec les lettres fournies"""
            sig_mot = signature(mot)
            "à compléter"


        #Tests unitaires de la fonction mot_possible
        def test_mot_possible():
            assert mot_possible("zazou", {"a" : 1, "b" : 1, "o" : 2, "u" : 1, "z" : 2})
            assert not mot_possible("zazou", {"b" : 1, "o" : 1, "u" : 1, "z" : 2})
            assert not mot_possible("zazou", {"a" : 1, "o" : 2, "u" : 3, "z" : 1})
        
        # %% Programme principal
        # saisie du nombre de mots du dictionnaire
        n = int(input())
        # saisie des mots du dictionnaire dans une liste
        dico = [input().rstrip() for _ in range(n)]
        letters = input().rstrip()
        sig_lettres = signature(letters)
        smax = -1
        mot_max = ""
        # boucle sur les mots du dictionnaire
        for mot in dico:
            "à compléter"


        print(mot_max)
        ~~~
    
    === "Solution"

        ~~~python
        import sys
        import math

        # Auto-generated code below aims at helping you parse
        # the standard input according to the problem statement.

        def signature(mot):
            sig  = dict()
            for c in mot:
                if c not in sig:
                    sig[c] = 1
                else:
                    sig[c] = sig[c] + 1
            return sig

        def score_mot(mot):
            s = 0
            for c in mot:
                s = s + valeur[c]
            return s

        def mot_possible(mot, sig_lettres):
            sig_mot = signature(mot)
            for c in sig_mot:
                if c not in sig_lettres or sig_mot[c] > sig_lettres[c]:
                    return False
            return True

        valeur = {
            'a' : 1,
            'e' : 1,
            'i' : 1,
            'o' : 1,
            'n' : 1,
            'r' : 1,
            't' : 1,
            'l' : 1,
            's' : 1,
            'u' : 1,
            'd' : 2,
            'g' : 2,
            'b' : 3, 'c' : 3, 'm' : 3, 'p' : 3,
            'f' : 4, 'h' : 4, 'v' : 4, 'w' : 4, 'y' : 4,
            'k' : 5, 
            'j' : 8, 'x' : 8,
            'q' : 10, 'z' : 10}


        n = int(input())
        dico = [input().rstrip() for _ in range(n)]
        letters = input().rstrip()
        sig_lettres = signature(letters)
        smax = -1
        for mot in dico:
            if mot_possible(mot, sig_lettres):
                score = score_mot(mot)
                if score > smax:
                    smax = score
                    mot_max = mot

        # Write an answer using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)

        print(mot_max)        
        ~~~

