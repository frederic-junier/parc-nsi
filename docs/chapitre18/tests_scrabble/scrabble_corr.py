#!/usr/bin/env python3
# -*- coding: utf-8 -*-

VALEUR = {
    "a": 1,
    "e": 1,
    "i": 1,
    "o": 1,
    "n": 1,
    "r": 1,
    "t": 1,
    "l": 1,
    "s": 1,
    "u": 1,
    "d": 2,
    "g": 2,
    "b": 3,
    "c": 3,
    "m": 3,
    "p": 3,
    "f": 4,
    "h": 4,
    "v": 4,
    "w": 4,
    "y": 4,
    "k": 5,
    "j": 8,
    "x": 8,
    "q": 10,
    "z": 10,
}


def signature(chaine):
    """
    Renvoie l'histogramme des lettres composant chaine.

    Parameters
    ----------
    mot : chaine de caractères de type str

    Returns
    -------
    sig : dictionnaire (clef=lettre, valeur=effectf)

    """
    sig = dict()
    for c in chaine:
        if c not in sig:
            sig[c] = 1
        else:
            sig[c] = sig[c] + 1
    return sig


assert signature("") == {}
assert signature("ananas") == {"a": 3, "n": 2, "s": 1}
assert signature("abcd") == {"a": 1, "b": 1, "c": 1, "d": 1}


def score_mot(mot):
    """
    Renvoie le score d'un mot.

    Parameters
    ----------
    mot : mot de type str

    Returns
    -------
    s : score du mot selon le barème VALEUR

    """
    s = 0
    for c in mot:
        s = s + VALEUR[c]
    return s


assert score_mot("") == 0
assert score_mot("zazou") == 23
assert score_mot("ananas") == 6


def mot_possible(mot, sig_lettres):
    """
    Détermine si on peut écrire mot avec l'histogramme sig_lettres.

    Parameters
    ----------
    mot : mot de type str
    sig_lettres : histogramme de lettres
                sous forme de dictionnaire (clef=lettre, valeur=effectf)

    Returns
    -------
    booléen
    """
    sig_mot = signature(mot)
    for c in sig_mot:
        if (c not in sig_lettres) or (sig_mot[c] > sig_lettres[c]):
            return False
    return True


assert mot_possible("zazou", {"a": 1, "b": 1, "o": 2, "u": 1, "z": 2})
assert not mot_possible("zazou", {"b": 1, "o": 1, "u": 1, "z": 2})
assert not mot_possible("zazou", {"a": 1, "o": 2, "u": 3, "z": 1})


def recherche_mot_max(lettres, liste_mots):
    """
    Renvoie le mot de liste_mots avec le score maximal pour les lettres.

    Parameters
    ----------
    lettres : chaine de caractères (str) avec les lettres tirées  sans espaces
    liste_mots : tableau de mots (type str) possibles

    Returns
    -------
    mot_max : mot réalisant le  score maximal de type str
    """
    mot_max = ""
    score_max = -1
    sig_lettres = signature(lettres)
    for mot in liste_mots:
        if mot_possible(mot, sig_lettres):
            score = score_mot(mot)
            if score_max <= score:
                score_max = score
                mot_max = mot
    return mot_max


liste_mots = [
    "amidons",
    "captes",
    "chatier",
    "empeser",
    "hatez",
    "missive",
    "missile",
    "defige",
    "raison",
    "raz",
]
assert recherche_mot_max("airsonz", liste_mots) == "raz"
assert recherche_mot_max("chatierz", liste_mots) == "hatez"
