#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random


VALEUR = {
    "a": 1,
    "e": 1,
    "i": 1,
    "o": 1,
    "n": 1,
    "r": 1,
    "t": 1,
    "l": 1,
    "s": 1,
    "u": 1,
    "d": 2,
    "g": 2,
    "b": 3,
    "c": 3,
    "m": 3,
    "p": 3,
    "f": 4,
    "h": 4,
    "v": 4,
    "w": 4,
    "y": 4,
    "k": 5,
    "j": 8,
    "x": 8,
    "q": 10,
    "z": 10,
}


ALPHABET = "".join([chr(ord("a") + k) for k in range(26)])


def signature(mot):
    sig = dict()
    for c in mot:
        if c not in sig:
            sig[c] = 1
        else:
            sig[c] = sig[c] + 1
    return sig


def score_mot(mot):
    s = 0
    for c in mot:
        s = s + VALEUR[c]
    return s


def mot_possible(mot, sig_lettres):
    sig_mot = signature(mot)
    for c in sig_mot:
        if (c not in sig_lettres) or (sig_mot[c] > sig_lettres[c]):
            return False
    return True


def recherche_mot_max(lettres, liste_mots):
    mot_max = ""
    score_max = -1
    sig_lettres = signature(lettres)
    for mot in liste_mots:
        if mot_possible(mot, sig_lettres):
            score = score_mot(mot)
            if score_max <= score:
                score_max = score
                mot_max = mot
    return mot_max


PREFIXE = "dico"
EXT = "txt"
TAILLE = 10000

f = open("dico.txt")
mots = [ligne.rstrip().lower() for ligne in f if len(ligne) <= 8]
f.close()
echantillon = sorted(random.sample(mots, TAILLE))
g = open(f"{PREFIXE}-{TAILLE}.{EXT}", encoding="utf8", mode="w")
for m in echantillon:
    g.write(m + "\n")
g.close()
for k in range(1, 11):
    mot_alea = random.choice(echantillon)
    print(mot_alea)
    liste_lettres = list(mot_alea)
    print(liste_lettres)
    random.shuffle(liste_lettres)
    lettres = "".join(liste_lettres)
    lettres_sup = [
        ALPHABET[random.randint(0, len(lettres) - 1)]
        for k in range(max(0, 7 - len(lettres)))
    ]
    lettres = lettres + "".join(lettres_sup)
    print(lettres)
    mot_max = recherche_mot_max(lettres, echantillon)
    h = open(f"ES-{TAILLE}-jeu-{k}.{EXT}", encoding="utf8", mode="w")
    h.write(lettres + "\n")
    h.write(mot_max)
    print(mot_max)
    h.close()

# for file in glob.glob(f"{PREFIXE}*.{EXT}"):
#     print(file)
