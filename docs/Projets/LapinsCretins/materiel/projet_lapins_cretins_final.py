#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Projet Lapins Cretins
Premiere NSI 2022/2023
"""

#%% DEBUT ETAPE 1


#%% Chiffres en base dix

def chiffres_base_dix(n):
    """
    Description et postcondition :
            Renvoie les chiffres de la  représentation de l'entier n >= 0 
            en base dix sous la forme d'un tableau avec les chiffres 
            par poids décroissant de gauche à droite
    Paramètres: 
             n est un entier
    Préconditions:
            n >= 0
    Retour:  un tableau de type list
    """
    t = []
    while n >= 10:
        c  = n % 10
        t = [c] + t
        n = n // 10
    t = [n] + t
    return t

def test_chiffres_base10():
    assert  chiffres_base_dix(0) == [0], "echec sur le cas chiffres_base_dix(0)"
    assert  chiffres_base_dix(8) == [8], "echec sur le cas chiffres_base_dix(8)"
    assert  chiffres_base_dix(10) == [1, 0], "echec sur le cas chiffres_base_dix(10)"
    assert  chiffres_base_dix(87) == [8, 7], "echec sur le cas chiffres_base_dix(87)"
    assert  chiffres_base_dix(807) == [8, 0, 7], "echec sur le cas chiffres_base_dix(807)"
    print("tests réussis")
    
    
#%% Conversions entre les bases dix et trois

#%% decimal_vers_ternaire

def decimal_vers_ternaire(n):
    """
    Description et postcondition :
        Renvoie le tableau des trois chiffres en base 3 de l'entier n en base 10        
    Paramètres: un entier n de type int
    Précondition :   0 <= n <= 26
    Retour: un tableau d'entiers  de type list      
    """
    # Préconditions
    assert isinstance(n, int)
    assert (0 <= n <= 26)
    # initialisation des chiffres ternaires
    chiffres_ternaire = [0, 0, 0]
    i = len(chiffres_ternaire) - 1
    # on applique l'algorithme des divisions en cascade
    # à compléter
    return chiffres_ternaire


def test_decimal_vers_ternaire():
    assert decimal_vers_ternaire(0) == [0, 0, 0], "echec sur decimal_vers_ternaire(0)"
    assert decimal_vers_ternaire(1) == [0, 0, 1], "echec sur decimal_vers_ternaire(1)"
    assert decimal_vers_ternaire(2) == [0, 0, 2], "echec sur decimal_vers_ternaire(2)"
    assert decimal_vers_ternaire(3) == [0, 1, 0], "echec sur decimal_vers_ternaire(3)"
    assert decimal_vers_ternaire(4) == [0, 1, 1], "echec sur decimal_vers_ternaire(4)"
    assert decimal_vers_ternaire(5) == [0, 1, 2], "echec sur decimal_vers_ternaire(5)"
    assert decimal_vers_ternaire(26) == [2, 2, 2], "echec sur decimal_vers_ternaire(26)"
    print("Tests réussis pour decimal_vers_ternaire")


#%% ternaire_vers_decimal

def ternaire_vers_decimal(tab):
    """
    Description et postcondition :
        Renvoie l'écriture décimale de l'entier n  compris entre 0 et 26
        dont les chiffres en base 3 sont le tableau tab        
    Paramètres: un tableau de trois entiers entre 0 et 2
    Préconditions :   
        tab de longueur 3
        Tous les éléments de tab sont des entiers entre 0 et 2
    Retour: un entier dec de type int      
    """
    # Préconditions
    assert len(tab) == 3
    assert all(e in [0, 1, 2] for e in tab)
    # initialisation de n
    dec = 0
    # à compléter
    # on applique l'algorithme d'Horner
    return dec


def test_ternaire_vers_decimal():
    assert ternaire_vers_decimal([0, 0, 0])  == 0, "echec sur ternaire_vers_decimal([0, 0, 0])"
    assert ternaire_vers_decimal([0, 0, 1]) == 1, "echec sur ternaire_vers_decimal([0, 0, 1])"
    assert ternaire_vers_decimal([0, 0, 2]) == 2,  "echec sur ternaire_vers_decimal([0, 0, 2])"
    assert ternaire_vers_decimal([0, 1, 0]) == 3, "echec sur ternaire_vers_decimal([0, 1, 0])"
    assert ternaire_vers_decimal([0, 1, 1]) == 4, "echec sur ternaire_vers_decimal([0, 1, 1])"
    assert ternaire_vers_decimal([0, 1, 2]) == 5, "echec sur ternaire_vers_decimal([0, 1, 2])"
    assert ternaire_vers_decimal([2, 2, 2]) == 26, "echec sur ternaire_vers_decimal([2, 2, 2])"
    print("Tests réussis pour ternaire_vers_decimal")



#%% Alphabet


def generer_symboles():
    """
    Description et postcondition :
        Renvoie une chaine de caractères constituée de l'espace 
        puis des 26 caractères alphabétiques majuscules
    Paramètres: aucun
    Retour: une  chaine de caractères de type str   
    """
    alph = " "
    return alph


def test_generer_symboles():
    assert generer_symboles() == ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    print("tests réussis pour generer_symboles")

def index_dans_sequence(elt, seq):
    """
    Description et postcondition :
            Renvoie l'index de la première occurrence de elt dans la séquence seq
            Renvoie len(seq) si elt pas dans seq
            Interdiction d'utiliser la méthode index       
    Paramètres: 
            elt de type quelconque
            seq est une séquence (str ou list)
    Préconditions:
            len(seq) >0
    Retour:   un entier entre 0 et len(symb) - 1 si car dans symb et len(symb) sinon   
    """
    # préconditions
    assert len(seq) > 0
    # fin préconditions
    for k in range(len(seq)):
        "à compléter"
    return len(seq)


def test_index_dans_sequence():
    assert index_dans_sequence('a', 'a') == 0, "echec sur index_dans_sequence('a', 'a')"
    assert index_dans_sequence('a', 'ab') == 0, "echec sur index_dans_sequence('a', 'ab')"
    assert index_dans_sequence('a', 'bba') == 2, "echec sur index_dans_sequence('a', 'bba')"
    assert index_dans_sequence('a', 'bbaaa') == 2, "echec sur index_dans_sequence('a', 'bbaaa')"
    assert index_dans_sequence('a', 'bc') == 2, "echec sur index_dans_sequence('a', 'bc')"
    assert index_dans_sequence('a', 'bbaaa') == 2, "echec sur index_dans_sequence('a', 'bbaaa')"
    assert index_dans_sequence('a', 'bc') == 2, "echec sur index_dans_sequence('a', 'bc')"
    assert index_dans_sequence(0, [1, 0, 2]) == 1, "echec sur index_dans_sequence(0, [1, 0, 2])"
    assert index_dans_sequence(1, [1, 0, 2]) == 0, "echec sur index_dans_sequence(1, [1, 0, 2])"
    assert index_dans_sequence(2, [1, 2, 2]) == 1, "echec sur index_dans_sequence(2, [1, 2, 2])"
    


#%% Codage / Decodage   texte <-> ternaire


def codage_texte_vers_ternaire(texte, symb):
    """
    Description et postcondition :
            Renvoie le codage en ternaire de l'index dans la chaine symb de chaque
            caratère  de la   chaine texte.
            Renvoie un tableau de tableaux de 3 entiers entre 0 et 2       
    Paramètres: 
             texte est une chaine de caractères
    Préconditions:
            tous les caractères de texte sont dans symb
            len(symb) == 27
    Retour:   un tableau de type list 
    """
    # Préconditions
    assert isinstance(symb, str) and isinstance(texte, str)
    assert len(symb) == 27
    # Fin préconditions
    codage = []
    # à compléter
    
    
def test_codage_texte_vers_ternaire():
    symb1 =  generer_symboles()
    attendu = [[0, 0, 2], [1, 2, 0], [1, 1, 2], [1, 0, 1], [1, 2, 0], [2, 1, 0], [2, 0, 0]]
    assert codage_texte_vers_ternaire("BONJOUR", symb1) == attendu
    print("Tests réussis pour codage_texte_vers_ternaire")


def decodage_ternaire_vers_texte(codage, symb):
    """
    Description et postcondition :
            Renvoie la chaine de caractères dont le codage en ternaire 
            par codage_texte_vers_ternaire("BONJOUR", symb) a donné codage
    Paramètres: 
             codage est un tableau de type list
             symb est une  chaine de caractères de type str
    Préconditions:
            codage est un tableau de tableaux constitués de 3 entiers entre 0 et 2
            len(symb) == 27
    Retour:  une chaine de caractères
    Exemples:
    >>> decodage_ternaire_vers_texte([[0, 0, 2], [1, 2, 0], [1, 1, 2], [1, 0, 1], [1, 2, 0], [2, 1, 0], [2, 0, 0]])
    'BONJOUR'
    """
    # Préconditions
    assert isinstance(symb, str)
    assert len(symb) == 27
    # Fin préconditions
    texte = ""
    # à compléter

def test_decodage_ternaire_vers_texte():
    symb1 =  generer_symboles()
    cod1 = [[0, 0, 2], [1, 2, 0], [1, 1, 2], [1, 0, 1], [1, 2, 0], [2, 1, 0], [2, 0, 0]]
    assert decodage_ternaire_vers_texte(cod1, symb1) == "BONJOUR"
    print("Tests réussis pour decodage_ternaire_vers_texte")
    
    
#%% FIN ETAPE 1
    

#%% DEBUT ETAPE 2

#%% Codage / Decodage   ternaire <-> lapin


def ternaire_vers_lapin(tab, traducteur):
    """
    Description et postcondition :
            Traduit en code lapin un élément de code ternaire
            tab est un tableau de trois entiers entre 0 et 2
            traducteur est un tableau de trois chaines de caractères
            Remvoie un tableau où pour tout index k d'un élément de tab
            tab[k] est remplacé par traducteur[tab[k]]
    Paramètres: 
             tab est un tableau de 3 entiers
             traducteur est un tableau de 3 chaines de caractères
    Préconditions:
            len(tab) == 3 and len(traducteur) == 3
            tous les éléments de tab sont entre 0 et 2
    Retour:  un tableau de chaines de caractères
    """
    # à compléter

def test_ternaire_vers_lapin():
    assert ternaire_vers_lapin([1, 0, 2], ['BWAA', 'BWAHA', 'BWA']) == ['BWAHA', 'BWAA', 'BWA']
    print("Tests réussis pour ternaire_vers_lapin")

def lapin_vers_ternaire(tab, traducteur):
    """
    Description et postcondition :
            Traduit en code ternaire un élement de code lapin
            tab est un tableau de trois chaines de caractères figurant dans traducteur
            traducteur est un tableau de trois chaines de caractères
            Remvoie un tableau où pour tout index k d'un élément de tab
            tab[k] est remplacé par son index dans traducteur[tab[k]]
    Paramètres: 
             tab est un tableau de 3 chaines de caractères
             traducteur est un tableau de 3 chaines de caractères distinctes
    Préconditions:
            len(tab) == 3 and len(traducteur) == 3
            tous les éléments de tab sont dans traducteur
    Retour:  un tableau de chaines de caractères
    """
    # à compléter
    
def test_lapin_vers_ternaire():
    assert lapin_vers_ternaire(['BWAHA', 'BWAA', 'BWA'], ['BWAA', 'BWAHA', 'BWA']) == [1, 0, 2]
    print("Tests réussis pour lapin_vers_ternaire")

    

def codage_ternaire_vers_lapin(code_ternaire, traducteur):
    """
    Description et postcondition :
            Traduit un code ternaire en  code lapin 
            en appliquant ternaire_vers_lapin(cod, traducteur) à chaque élément
            cod de code_ternaire            
            Remvoie un tableau de tableaux de trois chaines de caractères
            représentant le codage en code lapin. 
    Paramètres: 
             code_ternaire est un tableau de tableaux de 3 entiers entre 0 et 2
             représentant en code ternaire les caractères d'un message
             traducteur est un tableau de 3 chaines de caractères distinctes
    Préconditions:
            tous les éléments de code_ternaire sont des tableaux de longueur 3 constitués
            d'entiers entre 0 et 2
            len(traducteur) == 3
    Retour:  un tableau de tableaux de trois chaines de caractères
    représentant le codage en code lapin. 
    """  
    code_lapin = []
    # à compléter
    return code_lapin


def test_codage_ternaire_vers_lapin():
    attendu = [['BWA', 'BWA', 'BWAHA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAA', 'BWAA', 'BWAHA'], ['BWAA', 'BWA', 'BWAA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWA', 'BWA']] 
    assert codage_ternaire_vers_lapin([[0, 0, 2], [1, 2, 0], [1, 1, 2], [1, 0, 1], [1, 2, 0], [2, 1, 0], [2, 0, 0]], ["BWA", "BWAA", "BWAHA"]) == attendu
    print("Tests réussis pour codage_ternaire_vers_lapin")
    

def decodage_lapin_vers_ternaire(code_lapin, traducteur):
    """
    Description et postcondition :
            Traduit un code lapin en   code ternaire 
            en appliquant lapin_vers_ternaire(cod, traducteur) à chaque élément
            cod de (code_lapin           
            Remvoie un tableau de tableaux de trois entiers entre 0 et 2
            représentant le codage en code ternaire. 
    Paramètres: 
             code_lapin est un tableau de tableaux de 3 chaines de caractères
             représentant en code lapin les caractères d'un message
             traducteur est un tableau de 3 chaines de caractères distinctes
    Préconditions:
            tous les éléments de code_lapin sont des tableaux de longueur 3 constitués
            de chaines de caractères appartenant à traducteur
    Retour:  un tableau de tableaux de trois chaines de caractères
    représentant le codage en code lapin. 
    """  
    code_ternaire = []
    # à compléter
    return code_ternaire

    
def test_decodage_lapin_vers_ternaire():
    attendu = [[0, 0, 2], [1, 2, 0], [1, 1, 2], [1, 0, 1], [1, 2, 0], [2, 1, 0], [2, 0, 0]]
    assert decodage_lapin_vers_ternaire([['BWA', 'BWA', 'BWAHA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAA', 'BWAA', 'BWAHA'], ['BWAA', 'BWA', 'BWAA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWA', 'BWA']], ["BWA", "BWAA", "BWAHA"]) == attendu
    print("Tests réussis pour decodage_lapin_vers_ternaire")
    

#%% Codage / Decodage   texte <-> lapin



def codage_texte_vers_lapin(texte, symb, traducteur):
    """
    Description et postcondition :
            Traduit un texte en code lapin 
            Renvoie un tableau de tableaux de trois chaines de caractères représentant
            les codages des caractères de texte en code lapin
    Paramètres: 
             texte est de type str
             symb est de type str listant les caractères possibles dans texte
             traducteur est un tableau de 3 chaines de caractères distinctes
    Préconditions:
            tous les éléments de texte sont dans symb
            len(symb) == 27
            len(traducteur) == 3            
    Retour:  un tableau de tableaux de trois chaines de caractères
    représentant le codage en code lapin. 
    """  
    # préconditions
    assert isinstance(symb, str)
    assert len(symb) == 27    
    assert all(e in symb for e in texte)
    assert all(isinstance(e,str) for e in traducteur) 
    assert len(traducteur) == 3 and len(set(traducteur)) == 3    
    # fin préconditions
    # à compléter
    
    
def test_codage_texte_vers_lapin():
    symb1 = generer_symboles()
    attendu = [['BWA', 'BWA', 'BWAHA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAA', 'BWAA', 'BWAHA'], ['BWAA', 'BWA', 'BWAA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWA', 'BWA']]
    assert codage_texte_vers_lapin("BONJOUR", symb1, ["BWA", "BWAA", "BWAHA"]) == attendu
    print("Tests réussis pour codage_texte_vers_lapin")

def decodage_lapin_vers_texte(code_lapin, symb, traducteur):
    """
    Description et postcondition :
            Décode un  code lapin en texte
    Paramètres: 
             code_lapin est un tableau de tableaux de trois chaines de caracteres
             representant le codage d'un texte en code lapin
             symb est de type str listant les caractères possibles dans texte
             traducteur est un tableau de 3 chaines de caractères distinctes
    Préconditions:
            toutes les chaines dans  les éléments de code_lapin sont dans traducteur
            len(symb) == 27
            len(traducteur) == 3            
    Retour:  un texte
    """  
    # préconditions
    assert isinstance(symb, str)
    assert len(symb) == 27
    assert all(isinstance(e,str) for e in traducteur) 
    assert len(traducteur) == 3 and len(set(traducteur)) == 3
    assert all(all(f in traducteur for f in e) for e in code_lapin)    
    # fin préconditions
    # à compléter

def test_decodage_lapin_vers_texte():
    symb1 = generer_symboles()
    attendu = "BONJOUR"    
    assert decodage_lapin_vers_texte([['BWA', 'BWA', 'BWAHA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAA', 'BWAA', 'BWAHA'], ['BWAA', 'BWA', 'BWAA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWA', 'BWA']], symb1, ["BWA", "BWAA", "BWAHA"]) == attendu
    print("Tests réussis pour decodage_lapin_vers_texte")


#%% FIN ETAPE 2


#%% DEBUT ETAPE 3

#%% Permutations de ["BWA", "BWAA", "BWAHA"]

def permutations(tab):
    """
    Description et postcondition :
            Renvoie un tableau de tableaux contenant les 3 * 2 * 1 = 6 
            permutations des éléments de tab tableau de 3 élements
    Paramètres: 
            tab de type list
    Préconditions:
            len(tab) == 3            
    Retour:  un tableau de type list
    """     
    liste_perm = []
    for i in range(3):
        e1 = tab[i]
        for j in range(3):
            if j != i:
                e2 = tab[j]
                # à compléter
    return liste_perm


def test_permutations():
    attendu1 = sorted([[0, 1, 2], [0, 2, 1], [1, 0, 2], [1, 2, 0], [2, 0, 1], [2, 1, 0]])
    assert sorted(permutations([0, 1 , 2])) == attendu1
    attendu2 = [['BWA', 'BWAA', 'BWAHA'], ['BWA', 'BWAHA', 'BWAA'], ['BWAA', 'BWA', 'BWAHA'], ['BWAA', 'BWAHA', 'BWA'], ['BWAHA', 'BWA', 'BWAA'], ['BWAHA', 'BWAA', 'BWA']]
    assert sorted(permutations(["BWA", "BWAA", "BWAHA"])) == attendu2
    print("tests réussis pour test_permutations")
    
    
#%% Solution du pydefis https://pydefis.callicode.fr/defis/C22_BwaCode02/txt

def decoupage_bloc3(texte):
    """
    Description et postcondition :
        Découpe une chaine de caractères constituée de 3 * n mots 
        (appartenant à ['BWAHA', 'BWAHA', 'BWAA']) séparés par des espaces 
        en un tableau de tableaux   où les mots sont regroupés par blocs de 3            
    Paramètres: 
            texte de type str
    Préconditions:
            tous les mots de texte dans  ['BWAHA', 'BWAHA', 'BWAA']
    Retour:  un tableau de tableaux de taille 3
    """
    liste_blocs = []
    bloc = []
    mot = ""
    caracteres = "BWAHA"
    k = 0
    # à compléter
    
def test_decoupage_bloc3():
    attendu = [['BWA', 'BWA', 'BWAA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWAHA', 'BWAA']]
    assert decoupage_bloc3("BWA BWA BWAA BWAHA BWAA BWA BWAHA BWAHA BWAA") == attendu
    print("tests réussis pour decoupage_bloc3")

def liste_blocs_vers_texte(liste_blocs):
    """
    Fonction réciproque de      decoupage_bloc3(texte)
    Exemples:
    >>> liste_blocs_vers_texte([['BWA', 'BWA', 'BWAA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWAHA', 'BWAA']])
    "BWA BWA BWAA BWAHA BWAA BWA BWAHA BWAHA BWAA"    
    """
    # à compléter
            
def test_liste_blocs_vers_texte():
    attendu = "BWA BWA BWAA BWAHA BWAA BWA BWAHA BWAHA BWAA"
    assert liste_blocs_vers_texte([['BWA', 'BWA', 'BWAA'], ['BWAHA', 'BWAA', 'BWA'], ['BWAHA', 'BWAHA', 'BWAA']]) == attendu
    print("tests réussis pour liste_blocs_vers_text")



def solution_pydefis(path_input, path_output):
    """
    Ouvre le fichier de chemin path_input contenant le message en code lapin
    Ecrit dans le fichier de chemin path_output, pour toutes les permutations 
    possibles de 'BWA', 'BWA', 'BWAA']  (valeur de traducteur dans decodage_lapin_vers_texte(texten symb, traducteur)):
            le décodage du message contenu dans le fichier de chemin path_input
            le codage en code lapin du message 'STOP'
    """
    f = open(path_input)
    texte_source = f.read()
    f.close()
    code_lapin = decoupage_bloc3(texte_source)
    symboles = generer_symboles()
    g = open(path_output, mode="w")
    for traducteur in permutations(["BWA", "BWAA", "BWAHA"]):
        texte_decode = decodage_lapin_vers_texte(code_lapin, symboles, traducteur)
        g.write(texte_decode + "\n")
        g.write(liste_blocs_vers_texte(codage_texte_vers_lapin("STOP", symboles, traducteur)))
        g.write("\n\n\n\n")
    g.close()
        
    



def tout_tester():
    test_decimal_vers_ternaire()
    test_ternaire_vers_decimal()
    test_generer_symboles()
    test_index_dans_sequence()
    test_ternaire_vers_lapin()
    test_lapin_vers_ternaire()
    test_codage_ternaire_vers_lapin()
    test_decodage_lapin_vers_ternaire()
    test_codage_texte_vers_lapin()
    test_decodage_lapin_vers_texte()
    test_permutations()
    test_decoupage_bloc3()
    test_liste_blocs_vers_texte()
    print("Tous tests réussis")
    

#%% Programme principal
# Décommenter pour lancer tous les tests
#tout_tester()
# Décommenter pour résoudre le défi
#solution_pydefis("message_lapin.txt", "decodages_lapin.txt")
#%% FIN ETAPE 3