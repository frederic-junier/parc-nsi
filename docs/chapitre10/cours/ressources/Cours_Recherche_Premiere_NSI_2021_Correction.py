#!/usr/bin/env python
# coding: utf-8

# <div class = "alert alert-warning">
# Ce fichier  est un notebook Python.
# 
# Il comporte deux types de cellules :
# 
# * les cellules d'édition dans lesquelles vous pouvez saisir du texte éventuellement enrichi de mises en formes ou de liens hypertextes avec la syntaxe du langage HTML simplifié qui s'appelle Markdown. Voir http://daringfireball.net/projects/markdown/ pour la syntaxe de Markdown.
# 
# * les cellules de code où l'on peut saisir du code Python3 puis le faire exécuter avec la combinaison de touches `CTRL + RETURN`
# 
# Une cellule peut être éditée  de deux façons différentes :
# 
# * en mode _commande_ lorsqu'on clique sur sa marge gauche qui est surlignée alors en bleu, on peut alors  :
# 
#     - changer le type de la cellule en appuyant sur `m` pour passer en cellule Markdown ou sur `y` pour passer en cellule de code
#     
#     - insérer une cellule juste au-dessus en appuyant sur `a`
#     
#     - insérer une cellule juste en-dessous en appuyant sur `b`
#     
#     - couper la cellule en appuyant sur `x` etc ...
#     
# * en mode _édition_ lorsqu'on clique sur l'intérieur de la cellule.
# 
# L'aide complète sur les raccourcis claviers est accessible depuis le bouton `Help` dans la barre d'outils ci-dessus.
# <div>

# # Exercice 1

# In[2]:


def recherche_seq(tab, e):
    """
    Détermine si e dans tab

    Paramètres:
        tab : tableau d'entiers
        e : un entier

    Retour:
        booléen	
    """
    trouve = False
    for element in tab:
        if e == element:
            trouve = True
    return trouve



# Tests unitaires
assert not recherche_seq([], 1)
assert recherche_seq([1], 1)
assert not recherche_seq([0], 1)
assert not recherche_seq([-1, 2, 0], 1)
assert recherche_seq([-1, 2, 0], 0)
assert recherche_seq([-1, 2, 0], 2)
assert recherche_seq([-1, 2, 0], -1)
print("tests unitaires réussis")


# In[3]:



def recherche_seq2(tab, e):
    """
    Détermine si e dans tab

    Paramètres:
        tab : tableau d'entiers
        e : un entier

    Retour:
        booléen	
    """
    for element in tab:
        if e == element:
            return True
    return False




# Tests unitaires
assert not recherche_seq([], 1)
assert recherche_seq([1], 1)
assert not recherche_seq([0], 1)
assert not recherche_seq([-1, 2, 0], 1)
assert recherche_seq([-1, 2, 0], 0)
assert recherche_seq([-1, 2, 0], 2)
assert recherche_seq([-1, 2, 0], -1)
print("tests unitaires réussis")


# In[5]:


import time

for m in range(1, 8):   
    taille = 10**m
    tab = list(range(taille))
    e = taille + 1
    chrono = time.perf_counter()
    recherche_seq2(tab, e)
    print(f"taille = {taille}, temps(s) = {time.perf_counter() - chrono}")


# # Exercice 2

# In[7]:


def devinette(gauche, droite):
    """
    Paramètres : gauche et droite des entiers
    Précondition : 0 <= gauche < droite
    Valeur renvoyée : un entier, devine par dichotomie l'entier n choisi par 
    l'utilisateur avec gauche <= secret < droite
    """
    assert 0 <= gauche < droite
    while droite - gauche > 1:
        #variant droite - gauche - 2
        #invariant : gauche < droite
        if droite - gauche == 1:
            return gauche
        milieu = (gauche + droite) // 2
        reponse = int(input('Plus grand ou égal à ' + str(milieu) + ' (1 pour oui et 0 pour Non) ?'))
        if reponse == 1:
            gauche = milieu
        else:
            droite = milieu        
    return gauche


# Exemple de recherche de 9 dans $[0;2^{4}[$

# In[10]:


devinette(0, 2**4)


# # Exercice 4

# In[11]:


def est_croissant(tab):
    """
    Détermine si tab est dans l'ordre croissant    
    Paramètre : 
        tab un tableau d'entiers 
        Précondition : len(tab) > 0
    Retour : 
        un booléen
    """
    # précondition
    assert len(tab) > 0
    # à compléter
    for i in range(0, len(tab) - 1):
        if tab[i] > tab[i+1]:
            return False
    return True
    
# Tests unitaires
assert est_croissant([1])
assert est_croissant([1, 2])
assert est_croissant([-2, -1, 4])
assert not est_croissant([-2, -1, -3])
assert not est_croissant([-2,-3])
print("Tests unitaires réussis")


# In[9]:



def recherche_dicho_tab2(valeur, tab):
    """
    Renvoie l'index de première occurence de valeur dans tab
     ou -1 si valeur pas dans tab
    Paramètres : 
        valeur : un entier
        tab : un tableau d'entiers
        Précondition : len(tab) > 0
    Retour:
        un entier 
    """
    # précondition
    assert len(tab) > 0
    gauche, droite = 0, len(tab) - 1
    while droite - gauche >= 0:
        #invariant :  0 <= gauche et droite < len(tab)
        #invariant : element dans tab[gauche:droite + 1]
        milieu = (gauche + droite) // 2
        if tab[milieu] > valeur:
            droite = milieu - 1
        elif tab[milieu] < valeur:
            gauche = milieu + 1
        else:
            return milieu
    return -1


def recherche_dicho_tab2(valeur, tab):
    """
    Renvoie l'index d'une occurence de valeur dans tab
     ou -1 si valeur pas dans tab
    Paramètres : 
        valeur : un entier
        tab : un tableau d'entiers
        Précondition : len(tab) > 0
    Retour:
        un entier 
    """
    # précondition
    assert len(tab) > 0
    gauche, droite = 0, len(tab)
    while droite - gauche >= 1:
        #invariant :  0 <= gauche et droite <= len(tab)
        #invariant : element dans tab[gauche:droite]
        milieu = (gauche + droite) // 2
        if tab[milieu] > valeur:
            droite = milieu
        elif tab[milieu] < valeur:
            gauche = milieu + 1
        else:
            return milieu
    return -1




def recherche_dicho_tab_compteur(valeur, tab):
    """
    Renvoie un tuple :
    (index de première occurrence de valeur dans ta ou -1, compteur de comparaisons)
    Paramètres : 
        valeur : un entier
        tab : un tableau d'entiers
        Précondition : len(tab) > 0
    Retour:
        un tuple de deux entiers
    """
    # précondition
    assert len(tab) > 0
    gauche, droite = 0, len(tab) - 1
    compteur = 0
    while droite - gauche >= 0:
        #invariant :  0 <= gauche et droite < len(tab)
        #invariant : element dans tab[gauche:droite + 1]
        compteur += 1
        milieu = (gauche + droite) // 2
        if tab[milieu] > valeur:
            droite = milieu - 1
        elif tab[milieu] < valeur:
            gauche = milieu + 1
        else:
            return (milieu, compteur)
    return (-1, compteur)

def recherche_sequentielle(valeur, tab):
    """
    Renvoie l'index de première occurence de valeur dans tab
     ou -1 si valeur pas dans tab
    Paramètres : 
        valeur : un entier
        tab : un tableau d'entiers
        Précondition : len(tab) > 0
    Retour:
        un entier 
    """
    # précondition
    assert len(tab) > 0
    for k in range(len(tab)):
        if tab[k] == valeur:
            return k
    return -1

def recherche_python(valeur, tab):
    return valeur in tab


# tests unitaires
assert recherche_dicho_tab(2, [1,2, 2, 3]) in [1, 2]
assert recherche_dicho_tab(2, [1,2, 4, 5]) == 1
assert recherche_dicho_tab(2, [2, 4, 5]) == 0
assert recherche_dicho_tab(5, [2, 4, 5]) == 2
assert recherche_dicho_tab(3, [2, 4, 5]) == -1
assert recherche_dicho_tab(6, [2, 4, 5]) == -1
assert recherche_dicho_tab(0, [2, 4, 5]) == -1
print("tests unitaires réussis")


# # Exercice 5

# In[13]:


[(k, recherche_dicho_tab_compteur(2**k, list(range(2**k)))[1]) for k in range(1, 15)]
[(1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9), (9, 10), (11, 12), (12, 13), (13, 14), (14, 15)]


# 
# On peut conjecturer que dans le __pire des cas__ (recherche d'un élément qui n'est pas dans le tableau), la recherche dichotomique dans un tableau de taille $2^n$ coûte $n+1$ tours de boucles / comparaisons, c'est-à-dire le __nombre de chiffres en base deux__ de la taille du tableau $2^{n}$.
#     
#     
# __Preuve de cette conjecture :__
#    
# 
# Soit un tableau `tab` trié dans l'ordre croissant de taille $2^{n}$. 
# 
# 
# Supposons qu'on recherche dans `tab` un élément `e` n'appartenant pas à `tab`.
# 
# 
# Initialement la taille de la zone  de recherche est $2^{n}$.
# 
# A chaque itération, si la  zone de recherche est l'intervalle d'index $[g;d]$, alors on  on compare l'élément  d'index  médian $m=\left \lfloor \frac{g+d}{2} \right\rfloor$ avec `e` et comme `e` n'est pas dans `tab`, on poursuite la recherche dans l'intervalle d'index $\left[a;\left\lfloor \frac{g+d}{2} \right\rfloor\right]$ ou 
# $\left[\left\lfloor \frac{g+d}{2} \right\rfloor + 1; b\right]$ dont la taille est inférieure ou égale à la moitié de la zone de recherche précédente $\frac{g-d}{2}$.
# 
# Ainsi au bout de $n$ itérations la taille de la zone de recherche est inférieure ou égale à la taille de la zone de recherche initiale  divisée $n$ fois de suite par $2$ donc par $2^{n}$. Après $n$ itérations la taille de la zone de recherche est donc inférieure ou égale à $\frac{2^{n}}{2^{n}}=1$.  Il reste au plus $1$ élément donc la $(n+1)^{ième}$ itération termine l'algorithme en au plus $n+1$ comparaisons au total.

# In[4]:


def recherche_sequentielle(valeur, tab):
    """
    Renvoie l'index de première occurence de valeur dans tab
     ou -1 si valeur pas dans tab
    Paramètres : 
        valeur : un entier
        tab : un tableau d'entiers
        Précondition : len(tab) > 0
    Retour:
        un entier 
    """
    # précondition
    assert len(tab) > 0
    for k in range(len(tab)):
        if tab[k] == valeur:
            return k
    return -1

def recherche_python(valeur, tab):
    return valeur in tab


# In[5]:


import matplotlib.pyplot as plt
import numpy as np
import time 


def chronometre_pire_cas(f, taille_tableau):
    """
    Recherche avec la fonction  f un élément n'appartenant
    pas à un tableau de taille taille_tableau
    Renvoie le temps d'exécution
    """
    tab = list(range(taille_tableau))
    debut = time.perf_counter()
    rep = f(taille_tableau, tab)
    return time.perf_counter() - debut

def temps_moyen_pire_cas(f, taille_tableau, taille_echantillon):
    """
    Renvoie le temps d'exécution moyen d'une recherche avec f 
    dans le pire des cas sur un échantillon de taille_echantillon
    tableaux de taille taille_tableau
    """
    temps_cumul = 0
    for _ in range(taille_echantillon):
        temps_cumul = temps_cumul + chronometre_pire_cas(f, taille_tableau)
    return temps_cumul / taille_echantillon


def evolution_ratio_diff_pire_cas(f, benchmark):
    """
    Prend en paramètre une fonction de recherche dans un tableau (séquentielle ou dichotomique)
    Affiche les temps moyens d'exécution de la recherche dans le pire des cas 
    sur des échantillons de tableaux de même taille choisie dans une liste  benchmark de tailles croissantes. 
    Affiche aussi le quotient et la différence entre les temps moyens pour des tailles successives.    
    """
    ratio = 0
    difference = 0
    temps_precedent = -1
    for taille in benchmark:
        temps = temps_moyen_pire_cas(f,taille, 10)
        if temps_precedent > 0:
            ratio = temps / temps_precedent
            difference = temps - temps_precedent
        temps_precedent = temps
        print(f"{f.__name__}({taille},list(range({taille}))) en {temps:.1e} secondes,\n tps/tps_pre = {ratio:.1e} et tps - tps_pre = {difference:.1e}\n")
        

def graphique_temps_pire_cas(f, benchmark, chemin_fichier,semilogx):
    """
    Prend en paramètre une fonction de recherche dans un tableau (séquentielle ou dichotomique)
    Affiche un graphique avec les temps moyens d'exécution de la recherche dans le pire des cas
    sur des échantillons de tableaux de même taille choisie dans une liste  benchmark de tailles croissantes,
    en fonction des tailles   
    """
    tab_temps = [temps_moyen_pire_cas(f,taille, 10) for taille in benchmark]
    if semilogx:
        plt.semilogx(benchmark, tab_temps, marker='o', linestyle='', color='blue')
    else:
        plt.plot(benchmark, tab_temps, marker='o', linestyle='', color='blue')
    plt.xlabel('Taille du tableau')
    plt.ylabel("Temps d'exécution")
    plt.title(f"{f.__name__}")
    plt.savefig(chemin_fichier)
    plt.show()
    
def graphique_comparaison_temps_pire_cas(f1, f2, benchmark, chemin_fichier, semilogx, semilogy):
    """
    Prend en paramètre deux fonctions f1 et f2 de recherche dans un tableau.
    Exécute f1 et f2 sur des échantillons de tableaux de même taille dans le pire des cas.
    Affiche un graphique avec les temps de f1 (y) en fonction de ceux de f2 (x).
    semilogx et semilogy sont des booléens fixant des échelles logarithmiques
    ou linéaires sur les axes    
    """    
    ty = [temps_moyen_pire_cas(f1,taille, 10)  for taille in benchmark]
    tx = [temps_moyen_pire_cas(f2,taille, 10)  for taille in benchmark]
    if semilogx:
        plt.semilogx(tx, ty, marker='o', linestyle='', color='blue')
    elif semilogy:
        plt.semilogy(tx, ty, marker='o', linestyle='', color='blue')
    else:
        plt.plot(tx, ty, marker='o', linestyle='', color='blue')
    plt.xlabel('Temps recherche dicho')
    plt.ylabel("Temps recherche séquentielle")
    plt.savefig(chemin_fichier)
    plt.show()


# ⚠️  Exécuter le code Python ci-dessous à partir du fichier Python fourni dans Spyder, dans le navigateur les ressources sont trop limitées

# In[ ]:


evolution_ratio_diff_pire_cas(recherche_sequentielle, [10 ** k for k in range(3, 8)])
evolution_ratio_diff_pire_cas(recherche_dicho_tab, [10 ** k for k in range(3, 8)])
graphique_comparaison_temps_pire_cas(recherche_sequentielle, recherche_dicho_tab2, [2 ** k for k in range(5, 23)], 
  "comparaison_seq_dicho.png", semilogx = False, semilogy=True)


# ![comparaison recherches](https://capytale2.ac-paris.fr/web/sites/default/files/2021-12-06-18-44-56//mcer_VBB09066/comparaison_seq_dicho.png)

# Exécution du code précédent avec un interpréteur Python classique (hors du navigateur) :
#     
# ~~~python
# recherche_sequentielle(1000,list(range(1000))) en 9.2e-05 secondes,
#  tps/tps_pre = 0.0e+00 et tps - tps_pre = 0.0e+00
# 
# recherche_sequentielle(10000,list(range(10000))) en 1.0e-03 secondes,
#  tps/tps_pre = 1.1e+01 et tps - tps_pre = 9.1e-04
# 
# recherche_sequentielle(100000,list(range(100000))) en 6.7e-03 secondes,
#  tps/tps_pre = 6.7e+00 et tps - tps_pre = 5.7e-03
# 
# recherche_sequentielle(1000000,list(range(1000000))) en 6.8e-02 secondes,
#  tps/tps_pre = 1.0e+01 et tps - tps_pre = 6.1e-02
# 
# recherche_sequentielle(10000000,list(range(10000000))) en 6.8e-01 secondes,
#  tps/tps_pre = 1.0e+01 et tps - tps_pre = 6.2e-01
# 
# recherche_dicho_tab(1000,list(range(1000))) en 4.1e-06 secondes,
#  tps/tps_pre = 0.0e+00 et tps - tps_pre = 0.0e+00
# 
# recherche_dicho_tab(10000,list(range(10000))) en 5.3e-06 secondes,
#  tps/tps_pre = 1.3e+00 et tps - tps_pre = 1.2e-06
# 
# recherche_dicho_tab(100000,list(range(100000))) en 9.8e-06 secondes,
#  tps/tps_pre = 1.8e+00 et tps - tps_pre = 4.5e-06
# 
# recherche_dicho_tab(1000000,list(range(1000000))) en 1.9e-05 secondes,
#  tps/tps_pre = 1.9e+00 et tps - tps_pre = 8.8e-06
# 
# recherche_dicho_tab(10000000,list(range(10000000))) en 2.0e-05 secondes,
#  tps/tps_pre = 1.1e+00 et tps - tps_pre = 1.5e-06
# ~~~
#     
# L'expérience confirme  que  si on multiplie par 10 la taille du tableau, dans le __pire des cas__ :
#     
# * le temps d'exécution est multiplié par 10 pour une _recherche séquentielle_
# * le temps d'exécution augmente d'une constante pour une _recherche dichotomique_
#     
# Sur le graphique ci-dessous, l'échelle est loagrithmique en ordonnée (temps d'exécution de la recherche séquentielle pour un tableau de taille $n$) et linéaire en abscisse (temps d'exécution de la recherche dichotomique pour un tableau de taille $n$). Les points sont alignés donc  si on note $t_{s}$ le temps pour une recherche séquentielle  et $t_{d}$ le temps pour une recherche dichotomique, on peut conjecturer une relation de la forme  $t_{s}=10^{at_{d}+b}$ ou encore $\log(t_{s})=at_{d}+b$ où $\log$ est la fonction logarithme décimal donnant le nombre de chiffres en base dix (moin un). A une constante près cette relation se traduit par $\log_{2}(t_{s})=ut_{d}+v$ où $\log_{2}$ est la fonction logarithme binaire donnant le nombre de chiffres en base deux (moins un).
#     
# Si on admet que le temps d'exécution sur la même machine de ces deux algorithmes dépend du nombre de comparaisons effectués, sachant que le nombre de comparaisons pour la _recherche séquentielle_ dans le pire des cas est __linéaire__ (proportionnel à la taille $n$ du tableau), l'expérience nous permet de conjecturer que nombre de comparaisons pour pour la _recherche dichotomique_ dans le pire des cas est __logarithmique__ (proportionnel au nombre de chiffres en base 2 de la taille $n$ du tableau).
