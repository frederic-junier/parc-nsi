---
title:  Chapitre 17  architecture de Von Neumann
layout: parc
---



Cours de Frédéric Junier.




## Cours 

* [Cours version pdf](chapitre17/NSI-ArchitectureVonNeumann-Cours2022.pdf)
* [Emulateur Aqua](http://www.peterhigginson.co.uk/AQA/).
* Ressources :
    * [Archive avec les programmes pour l'exercice 5](chapitre17/ressources/exemple5.zip)
    * [Archive avec les corrigés des programmes d'assembleur](chapitre17/ressources/programmes_assembleur.zip)
    * [Correction video de l'exercice 7](https://nuage03.apps.education.fr/index.php/s/PZmdbwns2wW2AJ8)
    * [Correction video de l'exercice 10](https://nuage03.apps.education.fr/index.php/s/zwPMMFXXm6gzbCt)

## Video


* Le cours d'architecture en video sur la plateforme Lumni est très bien : 
    * La video : <https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs>
    * Le support de cours : <https://medias2ftv.akamaized.net/videosread/education/PDF/NSI-archi.pdf>