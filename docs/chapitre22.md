---
title:  Chapitre 22  algorithme de classification KNN
layout: parc
---




Cours de Frédéric Junier.

* [Cours (markdown)](chapitre22/knn-cours-git.md)
* [Cours (pdf)](chapitre22/knn-cours-.pdf)
* [TP (markdown)](chapitre22/TP/knn-tp-git.md)
* [TP (pdf)](chapitre22/TP/knn-tp-.pdf)
