---
title:  Chapitre 14  complexité
layout: parc
---





Cours de Pierre Duclosson et Frédéric Junier.




## Cours 

* [Cours version pdf](chapitre14/cours_complexite_2022.pdf)
* [Carnet Capytale avec corrigés des exercices](https://capytale2.ac-paris.fr/web/c/38c6-329322)
* Référence : <https://www.bigocheatsheet.com/>
* Une série d'articles du [blog Binaire](https://www.lemonde.fr/blog/binaire/2021/04/16/henri-potier-a-lecole-de-la-complexite/) sur la théorie de la complexité :

![tableau de complexité](https://asset.lemde.fr/prd-blogs/2021/03/ce3a8762-tableau2.jpg)


??? video

    Une video de [David Louapre](https://www.youtube.com/scienceetonnante) sur le problème `P = NP ?` qui éclaire la notion de complexité sur des exemples étudiés en cours de première NSI.

    <iframe allowfullscreen src="https://ladigitale.dev/digiplay/#/v/608afc02b0cbb" frameborder="0" width="700" height="510"></iframe>


